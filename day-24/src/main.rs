use commons::parse_input;
use std::collections::HashSet;
use Army::*;
use AttackOutcome::*;

fn main() {
    let groups = parse_input!(parser::fight).unwrap();
    println!(
        "Units left after the fight: {}",
        remaining_armies_after_fight(groups.clone())
    );
    println!(
        "Immune system units left after fixing the fight: {}",
        remaining_armies_after_fixed_fight(groups)
    );
}

fn remaining_armies_after_fight(groups: Vec<Group>) -> usize {
    let groups = fight(groups);
    groups.iter().map(|g| g.count).sum::<usize>()
}

fn remaining_armies_after_fixed_fight(groups: Vec<Group>) -> usize {
    let mut b = 1;
    loop {
        let mut groups = groups.clone();
        boost(&mut groups, b);
        groups = fight(groups);
        let infection = groups
            .iter()
            .filter(|g| g.army == Infection)
            .map(|g| g.count)
            .sum::<usize>();
        if infection == 0 {
            return groups.iter().map(|g| g.count).sum::<usize>();
        }
        b += 1;
    }
}

#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
enum Army {
    Immune,
    Infection,
}

#[derive(Clone, Debug, Eq, PartialEq)]
struct Group {
    id: usize,
    army: Army,
    count: usize,
    hp: usize,
    immune: HashSet<String>,
    weak: HashSet<String>,
    damage: usize,
    attack_type: String,
    initiative: usize,
}

impl Group {
    fn effective_power(&self) -> usize {
        self.count * self.damage
    }

    fn potential_damage_from(&self, attacker: &Self) -> usize {
        let multiplier = if self.immune.contains(&attacker.attack_type) {
            0
        } else if self.weak.contains(&attacker.attack_type) {
            2
        } else {
            1
        };
        multiplier * attacker.effective_power()
    }

    fn units_killed_by(&self, attacker: &Self) -> usize {
        self.count.min(self.potential_damage_from(attacker) / self.hp)
    }

    fn combat_rank(&self) -> (usize, usize) {
        (self.effective_power(), self.initiative)
    }
}

fn choose_targets(groups: &[Group]) -> Vec<(usize, usize)> {
    let mut targets: Vec<usize> = (0..groups.len()).collect();
    targets.sort_by_key(|&i| groups[i].combat_rank());
    let mut attackers = targets.clone();
    attackers.reverse();

    let mut result = vec![];

    for a in attackers {
        let attacker = &groups[a];
        if let Some((i, t, _)) = targets
            .iter()
            .enumerate()
            .filter_map(|(i, &t)| {
                let target = &groups[t];
                if target.army == attacker.army {
                    return None;
                }
                let potential_damage = target.potential_damage_from(attacker);
                if potential_damage == 0 {
                    return None;
                }
                Some((i, t, potential_damage))
            })
            .max_by_key(|&(_, _, potential_damage)| potential_damage)
        {
            result.push((a, t));
            targets.remove(i);
        }
    }

    result
}

enum AttackOutcome {
    Stalemate,
    Losses,
}

fn attack(mut groups: Vec<Group>) -> (AttackOutcome, Vec<Group>) {
    let mut targets = choose_targets(&groups);
    let mut outcome = Stalemate;
    targets.sort_by_key(|&(a, _)| &groups[a].initiative);
    for (a, t) in targets.into_iter().rev() {
        let killed = &groups[t].units_killed_by(&groups[a]);
        if *killed > 0 {
            outcome = Losses;
        }
        groups[t].count -= killed;
    }
    (outcome, groups.into_iter().filter(|g| g.count > 0).collect())
}

fn fight(mut groups: Vec<Group>) -> Vec<Group> {
    loop {
        match attack(groups) {
            (Stalemate, gs) => return gs,
            (Losses, gs) => groups = gs,
        }
    }
}

fn boost(groups: &mut Vec<Group>, amount: usize) {
    for mut g in groups.iter_mut().filter(|g| g.army == Immune) {
        g.damage += amount;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use lazy_static::lazy_static;
    use maplit::hashset;

    lazy_static! {
        static ref VALID_GROUP: Group = Group {
            id: 0,
            army: Infection,
            count: 1,
            hp: 1,
            immune: hashset! {},
            weak: hashset! {},
            damage: 1,
            attack_type: "hugs".to_string(),
            initiative: 1,
        };
    }

    #[test]
    fn test_boost() {
        let mut groups = vec![
            Group {
                army: Infection,
                damage: 1,
                ..VALID_GROUP.clone()
            },
            Group {
                army: Immune,
                damage: 1,
                ..VALID_GROUP.clone()
            },
        ];
        boost(&mut groups, 500);
        assert_eq!(
            vec![(Infection, 1), (Immune, 501)],
            groups.iter().map(|g| (g.army, g.damage)).collect::<Vec<_>>()
        );
    }
    #[test]
    fn test_fight() {
        let groups = fight(vec![
            Group {
                army: Infection,
                initiative: 2,
                count: 5,
                damage: 1,
                hp: 6,
                ..VALID_GROUP.clone()
            },
            Group {
                army: Immune,
                initiative: 1,
                count: 6,
                damage: 1,
                hp: 5,
                ..VALID_GROUP.clone()
            },
        ]);

        assert_eq!(
            vec![(Infection, 5)],
            groups.iter().map(|g| (g.army, g.count)).collect::<Vec<_>>()
        );
    }

    #[test]
    fn test_groups_attack_in_order_of_decreasing_initiative() {
        let groups = vec![
            Group {
                army: Infection,
                initiative: 2,
                count: 5,
                damage: 1,
                hp: 6,
                ..VALID_GROUP.clone()
            },
            Group {
                army: Immune,
                initiative: 1,
                count: 6,
                damage: 1,
                hp: 5,
                ..VALID_GROUP.clone()
            },
        ];

        let (_, groups) = attack(groups);

        // Because its initiative is higher, group 0 attacks first, killing one unit in group 1.
        // Group 1 attacks next, but because it has lost one unit, it does not deal enough damage to group 0 to kill an unit
        assert_eq!(vec![5, 5], groups.iter().map(|g| g.count).collect::<Vec<_>>());
    }

    #[test]
    fn test_groups_with_no_units_cannot_attack_and_are_removed_from_the_fight() {
        let groups = vec![
            Group {
                army: Infection,
                initiative: 2,
                count: 5,
                damage: 5,
                hp: 5,
                ..VALID_GROUP.clone()
            },
            Group {
                army: Immune,
                initiative: 1,
                count: 5,
                damage: 8,
                hp: 5,
                ..VALID_GROUP.clone()
            },
        ];

        let (_, groups) = attack(groups);

        // Group 0 attacks group 1 first, killing all units.
        // Group 1 is therefore removed from the fight, and doesn’t get an opportunity to deal group 0 any damage
        assert_eq!(
            vec![(Infection, 5)],
            groups.iter().map(|g| (g.army, g.count)).collect::<Vec<_>>()
        );
    }

    #[test]
    fn test_choose_targets() {
        let groups = vec![
            Group {
                army: Infection,
                count: 5,
                damage: 3,
                ..VALID_GROUP.clone()
            },
            Group {
                army: Immune,
                count: 8,
                damage: 16,
                ..VALID_GROUP.clone()
            },
        ];

        assert_eq!(
            vec![
                (1, 0), // Group 1 attacks first because it has a higher effective power. It attacks group 0
                (0, 1), // Next, group 0 attacks group 1
            ],
            choose_targets(&groups)
        )
    }

    #[test]
    fn test_groups_only_attack_if_they_can_deal_some_damage() {
        let groups = vec![
            Group {
                army: Infection,
                attack_type: "radiation".to_string(),
                count: 5,
                damage: 3,
                ..VALID_GROUP.clone()
            },
            Group {
                army: Immune,
                immune: hashset! {"radiation".to_string()},
                count: 8,
                damage: 16,
                ..VALID_GROUP.clone()
            },
        ];

        // Group 0 does not attack because it has no target worth attacking
        assert_eq!(vec![(1, 0)], choose_targets(&groups))
    }

    #[test]
    fn test_groups_attack_the_target_to_which_they_would_deal_the_most_damage() {
        let groups = vec![
            Group {
                army: Infection,
                attack_type: "radiation".to_string(),
                immune: hashset! {"radiation".to_string()},
                count: 5,
                damage: 3,
                ..VALID_GROUP.clone()
            },
            Group {
                army: Immune,
                attack_type: "radiation".to_string(),
                ..VALID_GROUP.clone()
            },
            Group {
                army: Immune,
                weak: hashset! {"radiation".to_string()},
                attack_type: "radiation".to_string(),
                ..VALID_GROUP.clone()
            },
            Group {
                army: Immune,
                attack_type: "radiation".to_string(),
                ..VALID_GROUP.clone()
            },
        ];

        // Group 0 attacks group 2 because it has a weakness to its attack type
        assert_eq!(vec![(0, 2)], choose_targets(&groups))
    }

    #[test]
    fn test_in_case_of_a_tie_groups_attack_the_target_with_the_largest_effective_power() {
        let groups = vec![
            Group {
                army: Infection,
                attack_type: "radiation".to_string(),
                immune: hashset! {"radiation".to_string()},
                count: 5,
                damage: 3,
                ..VALID_GROUP.clone()
            },
            Group {
                army: Immune,
                count: 5,
                damage: 3,
                attack_type: "radiation".to_string(),
                ..VALID_GROUP.clone()
            },
            Group {
                army: Immune,
                count: 5,
                damage: 8,
                attack_type: "radiation".to_string(),
                ..VALID_GROUP.clone()
            },
            Group {
                army: Immune,
                count: 4,
                damage: 3,
                attack_type: "radiation".to_string(),
                ..VALID_GROUP.clone()
            },
        ];

        // Group 0 attacks group 2 because it has the highest effective power (count × damage)
        assert_eq!(vec![(0, 2)], choose_targets(&groups))
    }

    #[test]
    fn test_in_case_of_a_second_tie_groups_attack_the_target_with_the_largest_initiative() {
        let groups = vec![
            Group {
                army: Infection,
                attack_type: "radiation".to_string(),
                immune: hashset! {"radiation".to_string()},
                count: 5,
                damage: 3,
                ..VALID_GROUP.clone()
            },
            Group {
                army: Immune,
                initiative: 5,
                attack_type: "radiation".to_string(),
                ..VALID_GROUP.clone()
            },
            Group {
                army: Immune,
                initiative: 8,
                attack_type: "radiation".to_string(),
                ..VALID_GROUP.clone()
            },
            Group {
                army: Immune,
                initiative: 2,
                attack_type: "radiation".to_string(),
                ..VALID_GROUP.clone()
            },
        ];

        // Group 0 attacks group 2 because it has the highest initiative
        assert_eq!(vec![(0, 2)], choose_targets(&groups))
    }

    #[test]
    fn test_groups_with_the_largest_effective_power_choose_their_target_first() {
        let groups = vec![
            Group {
                army: Infection,
                count: 5,
                damage: 3,
                attack_type: "radiation".to_string(),
                immune: hashset! {"radiation".to_string()},
                ..VALID_GROUP.clone()
            },
            Group {
                army: Infection,
                count: 5,
                damage: 8,
                attack_type: "radiation".to_string(),
                immune: hashset! {"radiation".to_string()},
                ..VALID_GROUP.clone()
            },
            Group {
                army: Infection,
                count: 2,
                damage: 3,
                attack_type: "radiation".to_string(),
                immune: hashset! {"radiation".to_string()},
                ..VALID_GROUP.clone()
            },
            Group {
                army: Immune,
                attack_type: "radiation".to_string(),
                ..VALID_GROUP.clone()
            },
        ];

        // Group 1 get to choose its target first because it has the highest effective power; the other groups have nobody left to attack
        assert_eq!(vec![(1, 3)], choose_targets(&groups))
    }

    #[test]
    fn test_as_a_tie_breaker_groups_with_a_larger_initiative_get_to_choose_their_target_first() {
        let groups = vec![
            Group {
                army: Infection,
                initiative: 2,
                attack_type: "radiation".to_string(),
                immune: hashset! {"radiation".to_string()},
                ..VALID_GROUP.clone()
            },
            Group {
                army: Infection,
                initiative: 8,
                attack_type: "radiation".to_string(),
                immune: hashset! {"radiation".to_string()},
                ..VALID_GROUP.clone()
            },
            Group {
                army: Infection,
                initiative: 6,
                attack_type: "radiation".to_string(),
                immune: hashset! {"radiation".to_string()},
                ..VALID_GROUP.clone()
            },
            Group {
                army: Immune,
                attack_type: "radiation".to_string(),
                ..VALID_GROUP.clone()
            },
        ];

        // Group 1 get to choose its target first because it has the highest initiative; the other groups have nobody left to attack
        assert_eq!(vec![(1, 3)], choose_targets(&groups))
    }

    #[test]
    fn test_ordering_by_effective_power() {
        let ep_120 = Group {
            count: 6,
            damage: 20,
            ..VALID_GROUP.clone()
        };
        let ep_100 = Group {
            count: 4,
            damage: 25,
            ..VALID_GROUP.clone()
        };
        assert!(ep_120.combat_rank() > ep_100.combat_rank());
    }

    #[test]
    fn test_ordering_by_initiative_when_same_effective_power() {
        let ep_100_i_20 = Group {
            count: 5,
            damage: 20,
            initiative: 20,
            ..VALID_GROUP.clone()
        };
        let ep_100_i_10 = Group {
            count: 20,
            damage: 5,
            initiative: 10,
            ..VALID_GROUP.clone()
        };
        assert!(ep_100_i_20.combat_rank() > ep_100_i_10.combat_rank());
    }

    #[test]
    fn test_ordering_when_same_effective_power_and_initiative() {
        let ep_100_i_10_fire = Group {
            count: 10,
            damage: 10,
            attack_type: "hugs".to_string(),
            initiative: 10,
            ..VALID_GROUP.clone()
        };
        let ep_100_i_10_radiation = Group {
            count: 20,
            damage: 5,
            attack_type: "radiation".to_string(),
            initiative: 10,
            ..VALID_GROUP.clone()
        };
        assert!(ep_100_i_10_fire.combat_rank() == ep_100_i_10_radiation.combat_rank());
    }

    #[test]
    fn test_units_killed() {
        let attacker = Group {
            count: 85,
            damage: 2,
            ..VALID_GROUP.clone()
        };
        let defending = Group {
            count: 33,
            hp: 55,
            ..VALID_GROUP.clone()
        };
        assert_eq!(3, defending.units_killed_by(&attacker));
    }

    #[test]
    fn test_all_units_killed() {
        let attacker = Group {
            count: 85,
            damage: 200,
            ..VALID_GROUP.clone()
        };
        let defending = Group {
            count: 33,
            hp: 55,
            ..VALID_GROUP.clone()
        };
        assert_eq!(33, defending.units_killed_by(&attacker));
    }

    #[test]
    fn test_potential_damage() {
        let attacker = Group {
            count: 79,
            damage: 332,
            ..VALID_GROUP.clone()
        };
        let defending = VALID_GROUP.clone();
        assert_eq!(
            attacker.effective_power(),
            defending.potential_damage_from(&attacker)
        );
    }

    #[test]
    fn test_potential_damage_to_immune_target() {
        let attacker = Group {
            count: 79,
            damage: 332,
            attack_type: "radiation".to_string(),
            ..VALID_GROUP.clone()
        };
        let defending = Group {
            immune: hashset! {"radiation".to_string()},
            ..VALID_GROUP.clone()
        };
        assert_eq!(0, defending.potential_damage_from(&attacker));
    }

    #[test]
    fn test_potential_damage_to_weak_target() {
        let attacker = Group {
            count: 79,
            damage: 332,
            attack_type: "radiation".to_string(),
            ..VALID_GROUP.clone()
        };
        let defending = Group {
            weak: hashset! {"radiation".to_string()},
            ..VALID_GROUP.clone()
        };
        assert_eq!(
            2 * attacker.effective_power(),
            defending.potential_damage_from(&attacker)
        );
    }

    #[test]
    fn test_effective_power() {
        let g = Group {
            count: 79,
            damage: 332,
            ..VALID_GROUP.clone()
        };
        assert_eq!(79 * 332, g.effective_power());
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use indoc::indoc;
    use lazy_static::lazy_static;
    use nom::types::CompleteStr;

    fn export(groups: &[Group]) -> Vec<(Army, usize, usize)> {
        let mut result = groups.iter().map(|g| (g.army, g.id, g.count)).collect::<Vec<_>>();
        result.sort();
        result
    }

    lazy_static! {
        static ref GROUPS: Vec<Group> = {
            parser::fight(CompleteStr(indoc!(
                "
                Immune System:
                17 units each with 5390 hit points (weak to radiation, bludgeoning) with an attack that does 4507 fire damage at initiative 2
                989 units each with 1274 hit points (immune to fire; weak to bludgeoning, slashing) with an attack that does 25 slashing damage at initiative 3

                Infection:
                801 units each with 4706 hit points (weak to radiation) with an attack that does 116 bludgeoning damage at initiative 1
                4485 units each with 2961 hit points (immune to radiation; weak to fire, cold) with an attack that does 12 slashing damage at initiative 4
                "
            ))).unwrap().1
        };
    }

    #[test]
    fn test_with_sample_data_from_problem_description() {
        let groups = GROUPS.clone();

        assert_eq!(
            vec![
                (Immune, 1, 17),
                (Immune, 2, 989),
                (Infection, 1, 801),
                (Infection, 2, 4485)
            ],
            export(&groups)
        );

        let (_, groups) = attack(groups);
        assert_eq!(
            vec![(Immune, 2, 905), (Infection, 1, 797), (Infection, 2, 4434)],
            export(&groups)
        );

        let (_, groups) = attack(groups);
        assert_eq!(
            vec![(Immune, 2, 761), (Infection, 1, 793), (Infection, 2, 4434)],
            export(&groups)
        );

        let (_, groups) = attack(groups);
        assert_eq!(
            vec![(Immune, 2, 618), (Infection, 1, 789), (Infection, 2, 4434)],
            export(&groups)
        );
    }

    #[test]
    fn test_remaining_armies_after_fight() {
        let groups = GROUPS.clone();
        assert_eq!(5216, remaining_armies_after_fight(groups));
    }

    #[test]
    fn test_remaining_armies_after_fixed_fight() {
        let groups = GROUPS.clone();
        assert_eq!(51, remaining_armies_after_fixed_fight(groups));
    }
}

mod parser {
    use super::Army::*;
    use super::{Army, Group};
    use commons::parser::uint;
    use nom::types::CompleteStr;
    use nom::{
        alt, call, char, delimited, do_parse, eol, is_not, many1, map, named, named_args, opt, pair,
        preceded, separated_list, tag, terminated, IResult,
    };
    use std::collections::HashSet;

    #[derive(Default)]
    struct Special {
        immune: HashSet<String>,
        weak: HashSet<String>,
    }

    named!(attack_types<CompleteStr, HashSet<String>>,
           map!(
               separated_list!(tag!(", "), is_not!(",);")),
               |ws| ws.into_iter().map(|s| s.to_string()).collect()
           )
    );

    named!(immunities<CompleteStr, HashSet<String>>, preceded!(tag!("immune to "), attack_types));
    named!(weaknesses<CompleteStr, HashSet<String>>, preceded!(tag!("weak to "), attack_types));

    named!(weaknesses_and_immunities<CompleteStr, Special>,
           map!(
               alt!(
                   map!(pair!(immunities, opt!(preceded!(tag!("; "), weaknesses))), |(i, w)| (i, w.unwrap_or_default())) |
                   map!(pair!(weaknesses, opt!(preceded!(tag!("; "), immunities))), |(w, i)| (i.unwrap_or_default(), w))
               ),
               |(immune, weak)| Special { immune, weak }
           )
    );

    fn make_group(
        army: Army,
        count: usize,
        hp: usize,
        special: Option<Special>,
        damage: usize,
        attack_type: String,
        initiative: usize,
    ) -> Group {
        let Special { immune, weak } = special.unwrap_or_default();
        Group {
            army,
            count,
            hp,
            immune,
            weak,
            damage,
            attack_type,
            initiative,
            id: 0,
        }
    }

    named_args!(group(army: Army)<CompleteStr, Group>,
        do_parse!(
            count:       uint                                                                >>
                         tag!(" units each with ")                                           >>
            hp:          uint                                                                >>
                         tag!(" hit points ")                                                >>
            special:     opt!(delimited!(char!('('), weaknesses_and_immunities, tag!(") "))) >>
                         tag!("with an attack that does ")                                   >>
            damage:      uint                                                                >>
                         char!(' ')                                                          >>
            attack_type: map!(is_not!(" "), |cs| cs.to_string())                             >>
                         tag!(" damage at initiative ")                                      >>
            initiative:  uint                                                                >>
            (make_group(army, count, hp, special, damage, attack_type, initiative))
        )
    );

    named_args!(groups(army: Army)<CompleteStr, Vec<Group>>,
        map!(
            many1!(terminated!(call!(group, army), eol)),
            |mut gs| {
                for (i, g) in gs.iter_mut().enumerate() {
                    g.id = i+1;
                }
                gs
            }
        )
    );

    #[rustfmt::skip]
    pub(super) fn fight(i: CompleteStr) -> IResult<CompleteStr, Vec<Group>> {
        do_parse!(i,
                       tag!("Immune System:")   >>
                       eol                      >>
            immune:    call!(groups, Immune)    >>
                       eol                      >>
                       tag!("Infection:")       >>
                       eol                      >>
            infection: call!(groups, Infection) >>
            ([immune, infection].concat())
        )
    }

    #[cfg(test)]
    mod tests {
        use super::*;
        use indoc::indoc;
        use maplit::hashset;

        #[test]
        fn test_parse_group() {
            let (_, group) = group(CompleteStr("17 units each with 5390 hit points with an attack that does 4507 fire damage at initiative 2"), Infection).unwrap();
            assert_eq!(
                Group {
                    id: 0,
                    army: Infection,
                    count: 17,
                    hp: 5390,
                    immune: hashset! {},
                    weak: hashset! {},
                    damage: 4507,
                    attack_type: "fire".to_string(),
                    initiative: 2
                },
                group
            );
        }

        #[test]
        fn test_parse_group_with_one_weakness() {
            let (_, group) = group(CompleteStr("801 units each with 4706 hit points (weak to radiation) with an attack that does 116 bludgeoning damage at initiative 1"), Immune).unwrap();
            assert_eq!(
                Group {
                    id: 0,
                    army: Immune,
                    count: 801,
                    hp: 4706,
                    immune: hashset! {},
                    weak: hashset! {"radiation".to_string()},
                    damage: 116,
                    attack_type: "bludgeoning".to_string(),
                    initiative: 1
                },
                group
            );
        }

        #[test]
        fn test_parse_group_with_multiple_weaknesses() {
            let (_, group) = group(CompleteStr("801 units each with 4706 hit points (weak to radiation, bludgeoning) with an attack that does 116 bludgeoning damage at initiative 1"), Immune).unwrap();
            assert_eq!(
                Group {
                    id: 0,
                    army: Immune,
                    count: 801,
                    hp: 4706,
                    immune: hashset! {},
                    weak: hashset! {"radiation".to_string(), "bludgeoning".to_string()},
                    damage: 116,
                    attack_type: "bludgeoning".to_string(),
                    initiative: 1
                },
                group
            );
        }

        #[test]
        fn test_parse_group_with_one_immunity() {
            let (_, group) = group(CompleteStr("801 units each with 4706 hit points (immune to radiation) with an attack that does 116 bludgeoning damage at initiative 1"), Infection).unwrap();
            assert_eq!(
                Group {
                    id: 0,
                    army: Infection,
                    count: 801,
                    hp: 4706,
                    immune: hashset! {"radiation".to_string()},
                    weak: hashset! {},
                    damage: 116,
                    attack_type: "bludgeoning".to_string(),
                    initiative: 1
                },
                group
            );
        }

        #[test]
        fn test_parse_group_with_immunity_and_weakness() {
            let (_, group) = group(CompleteStr("801 units each with 4706 hit points (immune to fire; weak to bludgeoning, slashing) with an attack that does 116 bludgeoning damage at initiative 1"), Infection).unwrap();
            assert_eq!(
                Group {
                    id: 0,
                    army: Infection,
                    count: 801,
                    hp: 4706,
                    immune: hashset! {"fire".to_string()},
                    weak: hashset! {"bludgeoning".to_string(), "slashing".to_string()},
                    damage: 116,
                    attack_type: "bludgeoning".to_string(),
                    initiative: 1
                },
                group
            );
        }

        #[test]
        fn test_parse_group_with_weakness_and_immunity() {
            let (_, group) = group(CompleteStr("801 units each with 4706 hit points (weak to fire; immune to bludgeoning, slashing) with an attack that does 116 bludgeoning damage at initiative 1"), Infection).unwrap();
            assert_eq!(
                Group {
                    id: 0,
                    army: Infection,
                    count: 801,
                    hp: 4706,
                    weak: hashset! {"fire".to_string()},
                    immune: hashset! {"bludgeoning".to_string(), "slashing".to_string()},
                    damage: 116,
                    attack_type: "bludgeoning".to_string(),
                    initiative: 1
                },
                group
            );
        }

        #[test]
        fn test_parse_fight() {
            let (_, fight) = fight(CompleteStr(indoc!(
                "
                Immune System:
                17 units each with 5390 hit points with an attack that does 4507 fire damage at initiative 2
                7 units each with 390 hit points with an attack that does 507 brain damage at initiative 21

                Infection:
                801 units each with 4706 hit points with an attack that does 116 bludgeoning damage at initiative 1
                "
            ))).unwrap();
            assert_eq!(
                vec![
                    Group {
                        id: 1,
                        army: Immune,
                        count: 17,
                        hp: 5390,
                        immune: hashset! {},
                        weak: hashset! {},
                        damage: 4507,
                        attack_type: "fire".to_string(),
                        initiative: 2
                    },
                    Group {
                        id: 2,
                        army: Immune,
                        count: 7,
                        hp: 390,
                        immune: hashset! {},
                        weak: hashset! {},
                        damage: 507,
                        attack_type: "brain".to_string(),
                        initiative: 21
                    },
                    Group {
                        id: 1,
                        army: Infection,
                        count: 801,
                        hp: 4706,
                        immune: hashset! {},
                        weak: hashset! {},
                        damage: 116,
                        attack_type: "bludgeoning".to_string(),
                        initiative: 1
                    },
                ],
                fight
            );
        }
    }
}
