use criterion::{criterion_group, criterion_main, Criterion, ParameterizedBenchmark};
use day_25::*;
use maplit::hashset;
use std::collections::HashSet;

fn hash_constellations<'a, T>(points: T) -> Vec<HashSet<Point>>
where
    T: IntoIterator<Item = &'a Point>,
{
    let mut result = vec![];
    for &p in points.into_iter() {
        let mut new_result = vec![];
        let mut constellation = hashset! {p};
        for c in result {
            if p.belongs_to(&c) {
                for p in c {
                    constellation.insert(p);
                }
            } else {
                new_result.push(c);
            }
        }
        new_result.push(constellation);
        result = new_result;
    }
    result
}

fn bench_constellations(c: &mut Criterion) {
    let input = read_input().unwrap();

    c.bench(
        "Constellations",
        ParameterizedBenchmark::new(
            "Hash",
            |b, input| b.iter(|| hash_constellations(input)),
            vec![input],
        )
        .with_function("Vec", |b, input| b.iter(|| constellations(input))),
    );
}

criterion_group!(benches, bench_constellations);
criterion_main!(benches);
