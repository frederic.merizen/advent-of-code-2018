use commons::{lines, parse_input, Result};
use std::collections::{HashMap, HashSet};

mod parser {
    use nom::types::CompleteStr;
    use nom::{is_not, map, IResult};

    pub(super) fn label(i: CompleteStr) -> IResult<CompleteStr, String> {
        map!(i, is_not!("\r\n"), |s| s.to_string())
    }
}

fn read_input<'a>() -> Result<'a, Vec<String>> {
    parse_input!(lines!(parser::label))
}

fn checksum<S>(lines: &[S]) -> u64
where
    S: AsRef<str>,
{
    let (twos, threes) = lines.iter().fold((0, 0), |(twos, threes), line| {
        let (two, three) = line
            .as_ref()
            .chars()
            .fold(HashMap::new(), |mut letter_counts, c| {
                letter_counts.entry(c).and_modify(|e| *e += 1).or_insert(1);
                letter_counts
            })
            .values()
            .fold((false, false), |(two, three), &c| {
                (two || c == 2, three || c == 3)
            });
        (twos + two as u64, threes + three as u64)
    });

    twos * threes
}

fn fabric_box_id<S>(ids: &[S]) -> Option<String>
where
    S: AsRef<str>,
{
    let mut candidates = HashSet::new();

    ids.iter().map(|id| id.as_ref()).find_map(|id| {
        id.char_indices()
            .map(|(i, _)| {
                id.char_indices()
                    .filter(|(j, _)| *j != i)
                    .map(|(_, c)| c)
                    .collect()
            })
            .collect::<HashSet<_>>()
            .into_iter()
            .find_map(|sub_id| candidates.replace(sub_id))
    })
}

fn main() {
    let input = read_input().unwrap();
    println!("Checksum: {}", checksum(&input));
    println!("Box id: {}", fabric_box_id(&input).unwrap());
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref LINES: Vec<String> = read_input().unwrap();
    }

    #[test]
    fn test_checksum() {
        assert_eq!(3952, checksum(&*LINES));
    }

    #[test]
    fn test_fabric_box_id() {
        assert_eq!("vtnikorkulbfejvyznqgdxpaw", &fabric_box_id(&*LINES).unwrap());
    }
}
