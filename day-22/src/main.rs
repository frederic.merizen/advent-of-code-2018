use commons::Error::InvalidInput;
use commons::{parse_input, Result};
use maplit::hashset;
use std::collections::HashSet;
#[cfg(test)]
use std::iter::once;
use Type::*;

fn main() {
    let problem = parse_input!(parser::problem_input).unwrap();
    let ProblemInput {
        target_x, target_y, ..
    } = problem;
    let caves = CaveSystem::new(target_x + 1, target_y + 1, problem);
    println!("Total risk level: {}", caves.risk_level());
    println!("Quickest path to friend: {}", exploration::quickest_path(problem));
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
enum Type {
    Rocky,
    Wet,
    Narrow,
}

impl Type {
    fn all_types() -> HashSet<Type> {
        hashset! {Rocky, Wet, Narrow}
    }

    fn from_risk_level<'a, T: Into<usize>>(risk_level: T) -> Result<'a, Self> {
        match risk_level.into() {
            0 => Ok(Rocky),
            1 => Ok(Wet),
            2 => Ok(Narrow),
            _ => Err(InvalidInput),
        }
    }

    #[cfg(test)]
    fn to_char(self) -> char {
        match self {
            Rocky => '.',
            Wet => '=',
            Narrow => '|',
        }
    }
}

struct CaveSystem {
    width: usize,
    depth: usize,
    erosion_levels: Vec<usize>,
}

fn erosion_level(depth: usize, geologic_index: usize) -> usize {
    (depth + geologic_index) % 20183
}

impl CaveSystem {
    fn new(
        width: usize,
        height: usize,
        ProblemInput {
            target_x,
            target_y,
            depth,
        }: ProblemInput,
    ) -> Self {
        assert!(target_x > 0, "Special case not implemented (x=0)");
        assert!(target_y > 0, "Special case not implemented (y=0)");
        assert!(width > target_x);
        assert!(height > target_y);

        let mut cs = CaveSystem {
            width,
            depth,
            erosion_levels: Vec::with_capacity(width * height),
        };

        for x in 0..width {
            cs.push(x * 16807);
        }

        cs.extend_to(target_y);

        cs.start_line(target_x, target_y);
        cs.push(0);
        cs.continue_line(target_x + 1, width);

        cs.extend_to(height);

        cs
    }

    fn push(&mut self, geologic_index: usize) {
        self.erosion_levels
            .push(erosion_level(self.depth, geologic_index));
    }

    fn extend_to(&mut self, height: usize) {
        for y in (self.erosion_levels.len() / self.width)..height {
            self.start_line(self.width, y);
        }
    }

    fn start_line(&mut self, width: usize, y: usize) {
        self.push(y * 48271);
        self.continue_line(1, width);
    }

    fn continue_line(&mut self, x: usize, width: usize) {
        for _ in x..width {
            let o = self.erosion_levels.len();
            self.push(self.erosion_levels[o - 1] * self.erosion_levels[o - self.width]);
        }
    }

    fn get_type(&mut self, x: usize, y: usize) -> Type {
        assert!(x < self.width);
        let offset = y * self.width + x;
        if offset >= self.erosion_levels.len() {
            self.extend_to(y + 1);
        }
        Type::from_risk_level(self.erosion_levels[offset] % 3).unwrap()
    }

    #[cfg(test)]
    fn dump(&self) -> String {
        self.erosion_levels
            .chunks(self.width)
            .flat_map(|c| {
                c.iter()
                    .map(|e| Type::from_risk_level(e % 3).unwrap().to_char())
                    .chain(once('\n'))
            })
            .collect()
    }

    fn risk_level(&self) -> usize {
        self.erosion_levels.iter().map(|e| e % 3).sum()
    }
}

mod exploration {
    use super::Type::*;
    use super::{CaveSystem, ProblemInput, Type};
    use lazy_static::lazy_static;
    use maplit::{hashmap, hashset};
    use std::cmp::Ordering;
    use std::collections::{BinaryHeap, HashMap, HashSet};
    use Equipment::*;

    #[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
    enum Equipment {
        ClimbingGear,
        Torch,
        Neither,
    }

    impl Equipment {
        fn all_equipment() -> HashSet<Equipment> {
            hashset! {ClimbingGear, Torch, Neither}
        }
    }

    #[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
    struct Position {
        x: usize,
        y: usize,
        equipment: Equipment,
    }

    #[derive(Clone, Copy, Eq, PartialEq)]
    struct Move {
        cost: usize,
        pos: Position,
    }

    impl PartialOrd for Move {
        fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
            Some(self.cmp(other))
        }
    }

    impl Ord for Move {
        fn cmp(&self, other: &Self) -> Ordering {
            (other.cost, self.pos).cmp(&(self.cost, other.pos))
        }
    }

    #[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
    struct Transition {
        new_equipment: Equipment,
        cost: usize,
    }

    lazy_static! {
        static ref COMMON_EQUIPMENT: HashMap<(Type, Type), Equipment> = {
            let mut common_equipment = hashmap! {};
            for from in Type::all_types() {
                for to in Type::all_types() {
                    let mut compatible_equipment = Equipment::all_equipment();
                    compatible_equipment.remove(&from.incompatible_equipment());
                    compatible_equipment.remove(&to.incompatible_equipment());
                    let equipment = compatible_equipment.into_iter().next().unwrap();
                    common_equipment.insert((from, to), equipment);
                }
            }
            common_equipment
        };
    }

    impl Type {
        fn incompatible_equipment(self) -> Equipment {
            match self {
                Rocky => Neither,
                Wet => Torch,
                Narrow => ClimbingGear,
            }
        }

        fn transition(self, other: Self, current_equipment: Equipment) -> Transition {
            let (new_equipment, cost) = if current_equipment != other.incompatible_equipment() {
                (current_equipment, 1)
            } else {
                // We could shave off a few milliseconds with a hard-coded match
                // but this hash lookup is fast enough
                (COMMON_EQUIPMENT[&(self, other)], 8)
            };
            Transition { new_equipment, cost }
        }
    }

    struct Exploration {
        candidates: BinaryHeap<Move>,
        cave: CaveSystem,
        seen: HashSet<Position>,
        worst_outcome: usize,
        target_x: usize,
        target_y: usize,
    }

    impl Exploration {
        fn new(cave: CaveSystem, target_x: usize, target_y: usize) -> Self {
            let mut candidates = BinaryHeap::new();
            let seen = hashset! {};
            candidates.push(Move {
                pos: Position {
                    x: 0,
                    y: 0,
                    equipment: Torch,
                },
                cost: 0,
            });

            Exploration {
                candidates,
                cave,
                seen,
                target_x,
                target_y,
                worst_outcome: std::usize::MAX,
            }
        }

        fn transition(&mut self, from: Position, to_x: usize, to_y: usize) -> Transition {
            assert!(
                (from.x == to_x && ((from.y == to_y + 1) || (to_y == from.y + 1)))
                    || (from.y == to_y && ((from.x == to_x + 1) || (to_x == from.x + 1)))
            );
            let from_type = self.cave.get_type(from.x, from.y);
            let to_type = self.cave.get_type(to_x, to_y);
            from_type.transition(to_type, from.equipment)
        }

        fn push_next_moves(&mut self, Move { cost, pos }: Move) {
            for (dx, dy) in [(1, 0), (0, 1), (-1, 0), (0, -1)].iter() {
                let x = pos.x as isize + dx;
                let y = pos.y as isize + dy;
                if x < 0 || y < 0 {
                    continue;
                }
                let x = x as usize;
                let y = y as usize;
                if x >= self.cave.width {
                    continue;
                }
                let t = self.transition(pos, x, y);
                let pos = Position {
                    x,
                    y,
                    equipment: t.new_equipment,
                };

                // This check is a peformance optimization, not required for correctness
                if !self.seen.contains(&pos) {
                    self.candidates.push(Move {
                        cost: cost + t.cost,
                        pos,
                    })
                }
            }
        }

        fn play_finishing_move(&mut self, Move { cost, pos }: Move) -> Option<usize> {
            if pos.equipment == Torch {
                Some(cost)
            } else {
                self.candidates.push(Move {
                    pos: Position {
                        equipment: Torch,
                        ..pos
                    },
                    cost: cost + 7,
                });
                None
            }
        }

        fn can_beat_wort_outcome(&mut self, m: Move) -> bool {
            let Position { x, y, .. } = m.pos;

            // Optimization: do not consider plans that cannot possibly do better
            // than our best pessimistic plan so far.
            let dist = (x as isize - self.target_x as isize).abs() as usize
                + (y as isize - self.target_y as isize).abs() as usize;
            if dist + m.cost > self.worst_outcome {
                return false;
            }

            // Pessimistic estimate of work remaining:
            // - one equipment change per step
            // - plus one final equipment change on target
            self.worst_outcome = self.worst_outcome.min(m.cost + 8 * dist + 7);
            true
        }

        fn play_next_move(&mut self) -> Option<usize> {
            let m = self.candidates.pop()?;

            // `Seen` status was already checked when inserting in `push_next_moves`,
            // but the position can have been discovered in the meantime so we need
            // to check again
            if !self.seen.insert(m.pos) || !self.can_beat_wort_outcome(m) {
                return None;
            }

            let Position { x, y, .. } = m.pos;

            if (x, y) == (self.target_x, self.target_y) {
                self.play_finishing_move(m)
            } else {
                self.push_next_moves(m);
                None
            }
        }

        fn quickest_path(&mut self) -> usize {
            loop {
                if let Some(result) = self.play_next_move() {
                    return result;
                }
            }
        }
    }

    fn safe_width(target_x: usize, target_y: usize) -> usize {
        1 + target_x + (1 + target_y) * 7 / 2
    }

    pub(super) fn quickest_path(input: ProblemInput) -> usize {
        let ProblemInput {
            target_x, target_y, ..
        } = input;
        let width = safe_width(target_x, target_y);
        let cave_system = CaveSystem::new(width, target_y + 1, input);
        let mut exploration = Exploration::new(cave_system, target_x, target_y);
        exploration.quickest_path()
    }

    #[cfg(test)]
    mod tests {
        use super::*;
        use indoc::indoc;

        #[test]
        fn test_safe_width() {
            assert_eq!(46, safe_width(10, 9));
        }

        #[test]
        fn test_quickest_path() {
            assert_eq!(
                45,
                quickest_path(ProblemInput {
                    depth: 510,
                    target_x: 10,
                    target_y: 10
                })
            );
        }

        #[test]
        fn test_transition_to_same_terrain() {
            assert_eq!(
                Transition {
                    cost: 1,
                    new_equipment: Torch
                },
                Rocky.transition(Rocky, Torch)
            );
        }

        #[test]
        fn test_transition_to_terrain_with_compatible_equipment() {
            assert_eq!(
                Transition {
                    cost: 1,
                    new_equipment: Torch
                },
                Rocky.transition(Narrow, Torch)
            );
        }

        #[test]
        fn test_transition_to_terrain_with_incompatible_equipment() {
            assert_eq!(
                Transition {
                    cost: 8,
                    new_equipment: ClimbingGear
                },
                Rocky.transition(Wet, Torch)
            );
        }

        #[test]
        fn test_exploration_transition() {
            let cave = CaveSystem::new(
                3,
                3,
                ProblemInput {
                    target_x: 2,
                    target_y: 2,
                    depth: 510,
                },
            );

            assert_eq!(
                indoc!(
                    "
                    .=.
                    .|=
                    .=.
                    "
                ),
                cave.dump()
            );

            let mut exploration = Exploration::new(cave, 2, 2);

            assert_eq!(
                Transition {
                    cost: 1,
                    new_equipment: ClimbingGear
                },
                exploration.transition(
                    Position {
                        x: 0,
                        y: 0,
                        equipment: ClimbingGear
                    },
                    0,
                    1
                )
            );

            assert_eq!(
                Transition {
                    cost: 1,
                    new_equipment: ClimbingGear
                },
                exploration.transition(
                    Position {
                        x: 0,
                        y: 0,
                        equipment: ClimbingGear
                    },
                    1,
                    0
                )
            );

            assert_eq!(
                Transition {
                    cost: 8,
                    new_equipment: Neither,
                },
                exploration.transition(
                    Position {
                        x: 1,
                        y: 0,
                        equipment: ClimbingGear
                    },
                    1,
                    1
                )
            );
        }

        #[test]
        fn test_moves_are_ordered_by_decreasing_cost() {
            let move_1 = Move {
                cost: 10,
                pos: Position {
                    x: 10,
                    y: 10,
                    equipment: Neither,
                },
            };

            let move_2 = Move {
                cost: 1,
                pos: Position {
                    x: 1,
                    y: 1,
                    equipment: ClimbingGear,
                },
            };

            assert!(move_1 < move_2);
        }
    }
}
#[cfg(test)]
mod tests {
    use super::*;
    use indoc::indoc;

    #[test]
    fn test_generate_terrain() {
        let cs = CaveSystem::new(
            11,
            11,
            ProblemInput {
                target_x: 10,
                target_y: 10,
                depth: 510,
            },
        );
        assert_eq!(
            indoc!(
                "
                .=.|=.|.|=.
                .|=|=|||..|
                .==|....||=
                =.|....|.==
                =|..==...=.
                =||.=.=||=|
                |.=.===|||.
                |..==||=.|=
                .=..===..=|
                .======|||=
                .===|=|===.
                "
            ),
            cs.dump()
        );
    }

    #[test]
    fn test_generate_larger_terrain() {
        let cs = CaveSystem::new(
            15,
            15,
            ProblemInput {
                target_x: 10,
                target_y: 10,
                depth: 510,
            },
        );
        assert_eq!(
            indoc!(
                "
                .=.|=.|.|=.|=|=
                .|=|=|||..|.=..
                .==|....||=..|=
                =.|....|.==.|==
                =|..==...=.|==.
                =||.=.=||=|=..|
                |.=.===|||..=..
                |..==||=.|==|==
                .=..===..=|.|||
                .======|||=|=.|
                .===|=|===.===|
                =|||...|==..|=.
                =.=|=.=..=.||==
                ||=|=...|==.=|=
                |=.=||===.|||==
                "
            ),
            cs.dump()
        );
    }

    #[test]
    fn test_get_type() {
        let mut cs = CaveSystem::new(
            3,
            3,
            ProblemInput {
                target_x: 2,
                target_y: 2,
                depth: 510,
            },
        );

        assert_eq!(
            indoc!(
                "
                .=.
                .|=
                .=.
                "
            ),
            cs.dump()
        );
        assert_eq!(Rocky, cs.get_type(0, 0));
        assert_eq!(Narrow, cs.get_type(1, 1));
        assert_eq!(Wet, cs.get_type(2, 1));

        // dynamically extend cave system
        assert_eq!(Wet, cs.get_type(0, 3));
    }

    #[test]
    fn test_risk_level() {
        let cs = CaveSystem::new(
            11,
            11,
            ProblemInput {
                target_x: 10,
                target_y: 10,
                depth: 510,
            },
        );

        assert_eq!(114, cs.risk_level());
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
struct ProblemInput {
    depth: usize,
    target_x: usize,
    target_y: usize,
}

mod parser {
    use super::ProblemInput;
    use commons::parser::uint;
    use nom::types::CompleteStr;
    use nom::{char, do_parse, eol, tag, IResult};

    #[rustfmt::skip]
    pub(super) fn problem_input(i: CompleteStr) -> IResult<CompleteStr, ProblemInput> {
        do_parse!(i,
                      tag!("depth: ")  >>
            depth:    uint             >>
                      eol              >>
                      tag!("target: ") >>
            target_x: uint             >>
                      char!(',')       >>
            target_y: uint             >>
                      eol              >>
            (ProblemInput{ depth, target_x, target_y })
        )
    }

    #[cfg(test)]
    mod tests {
        use super::*;
        use indoc::indoc;

        #[test]
        fn test_parse_problem_input() {
            assert_eq!(
                Ok((
                    CompleteStr(""),
                    ProblemInput {
                        depth: 15,
                        target_x: 27,
                        target_y: 13
                    }
                )),
                problem_input(CompleteStr(indoc!(
                    "
                    depth: 15
                    target: 27,13
                    "
                )))
            )
        }
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref PARAMS: ProblemInput = { parse_input!(parser::problem_input).unwrap() };
    }

    #[test]
    fn test_risk_level() {
        let ProblemInput {
            target_x, target_y, ..
        } = *PARAMS;
        let caves = CaveSystem::new(target_x + 1, target_y + 1, *PARAMS);
        assert_eq!(8681, caves.risk_level());
    }
}
