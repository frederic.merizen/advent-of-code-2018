use commons::{parse_input, Result};

fn cell_level(grid_serial: i32, x: usize, y: usize) -> i32 {
    let rack_id = x as i32 + 10;
    ((rack_id * y as i32 + grid_serial) * rack_id / 100) % 10 - 5
}

#[derive(Clone)]
struct Grid {
    buffer: Vec<i32>,
    col_sum: Vec<i32>,
}

impl Grid {
    fn new(serial: i32) -> Self {
        let mut buffer = vec![0; 300 * 300];
        let mut o = 0;
        for y in 1..=300 {
            for x in 1..=300 {
                buffer[o] = cell_level(serial, x, y);
                o += 1;
            }
        }
        Grid {
            buffer,
            col_sum: vec![0; 300],
        }
    }

    fn max(&self, size: usize) -> (usize, i32) {
        let mut sum = self.col_sum.iter().take(size).sum();
        let mut max_val = sum;
        let mut max_pos = 0;
        for x in 0..(300 - size) {
            sum += self.col_sum[x + size] - self.col_sum[x];
            if sum > max_val {
                max_val = sum;
                max_pos = x + 1;
            }
        }
        (max_pos, max_val)
    }

    fn reset_accum(&mut self) {
        for x in 0..300 {
            self.col_sum[x] = 0;
        }
    }

    fn accum_line(&mut self, y: usize) {
        for x in 0..300 {
            self.col_sum[x] += self.buffer[x + 300 * y];
        }
    }

    fn deccum_line(&mut self, y: usize) {
        for x in 0..300 {
            self.col_sum[x] -= self.buffer[x + 300 * y];
        }
    }

    fn largest_square_sized(&mut self, size: usize) -> ((usize, usize), i32) {
        self.reset_accum();
        for y in 0..size {
            self.accum_line(y);
        }

        let (x, mut max_level) = self.max(size);
        let mut max_pos = (x + 1, 1);

        for y in 0..(300 - size) {
            self.deccum_line(y);
            self.accum_line(y + size);
            let (x, level) = self.max(size);
            if level > max_level {
                max_level = level;
                max_pos = (x + 1, y + 2);
            }
        }

        (max_pos, max_level)
    }

    fn largest_square(&mut self) -> ((usize, usize), usize) {
        let (s, (p, _)) = (1..300)
            .map(|s| (s, self.largest_square_sized(s)))
            .max_by_key(|(_, (_, l))| *l)
            .unwrap();
        (p, s)
    }
}

mod parser {
    use commons::parser::uint;
    use nom::types::CompleteStr;
    use nom::{eol, terminated, IResult};

    pub(super) fn input(i: CompleteStr) -> IResult<CompleteStr, i32> {
        terminated!(i, uint, eol)
    }
}

fn read_input<'a>() -> Result<'a, i32> {
    parse_input!(parser::input)
}

fn main() {
    let mut grid = Grid::new(read_input().unwrap());
    let ((x, y), _) = grid.largest_square_sized(3);
    println!("Largest cell is at {},{}", x, y);
    let ((x, y), s) = grid.largest_square();
    println!("Largest cell is at {},{},{}", x, y, s);
}

#[cfg(test)]
mod tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref G18: Grid = Grid::new(18);
        static ref G42: Grid = Grid::new(42);
    }

    #[test]
    fn test_cell_level() {
        assert_eq!(4, cell_level(8, 3, 5));
        assert_eq!(-5, cell_level(57, 122, 79));
        assert_eq!(0, cell_level(39, 217, 196));
        assert_eq!(4, cell_level(71, 101, 153));
    }

    #[test]
    fn test_build_buffer() {
        let mut g18 = G18.clone();
        assert_eq!(cell_level(18, 1, 1), g18.buffer[0]);
        assert_eq!(cell_level(18, 34, 68), g18.buffer[300 * (68 - 1) + 34 - 1]);
        g18.accum_line(0);
        assert_eq!(cell_level(18, 1, 1), g18.col_sum[0]);
    }

    #[test]
    fn test_max() {
        let mut g18 = G18.clone();
        g18.reset_accum();
        g18.accum_line(1);
        let expected: i32 = g18.col_sum.windows(17).map(|s| s.iter().sum()).max().unwrap();
        assert_eq!(expected, g18.max(17).1);

        g18.reset_accum();
        g18.accum_line(25);
        let expected: i32 = g18.col_sum.windows(99).map(|s| s.iter().sum()).max().unwrap();
        assert_eq!(expected, g18.max(99).1);
    }

    #[test]
    fn test_largest_square_sized() {
        let mut g18 = G18.clone();
        let mut g42 = G42.clone();
        assert_eq!(((33, 45), 29), g18.largest_square_sized(3));
        assert_eq!(((21, 61), 30), g42.largest_square_sized(3));
        assert_eq!(((90, 269), 113), g18.largest_square_sized(16));
        assert_eq!(((232, 251), 119), g42.largest_square_sized(12));
    }

    #[test]
    fn test_largest_square() {
        let mut g18 = G18.clone();
        let mut g42 = G42.clone();
        assert_eq!(((90, 269), 16), g18.largest_square());
        assert_eq!(((232, 251), 12), g42.largest_square());
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref GRID: Grid = Grid::new(7803);
    }

    #[test]
    fn test_largest_square_sized() {
        let mut grid = GRID.clone();
        assert_eq!(((20, 51), 31), grid.largest_square_sized(3));
    }

    #[test]
    fn test_largest_square() {
        let mut grid = GRID.clone();
        assert_eq!(((230, 272), 17), grid.largest_square());
    }
}
