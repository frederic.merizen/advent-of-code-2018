use commons::{parse_input, Result};

fn main() {
    let s = read_input().unwrap();
    println!("After 20 iterations: {}", sum_after_n_gens(&mut s.clone(), 20));
    println!(
        "After 50 000 000 000 iterations: {}",
        sum_after_n_gens(&mut s.clone(), 50_000_000_000)
    );
}

fn sum_after_n_gens(s: &mut State, n: usize) -> isize {
    let mut offset = 0;
    for i in 0..n {
        let old_offset = s.offset;
        s.update();
        let (old_plants, old_o) = plants(&s.next_plants);
        let (new_plants, new_o) = plants(&s.plants);
        if old_plants == new_plants {
            let drift = new_o + s.offset - old_o - old_offset;
            offset = (n - i - 1) as isize * drift;
            break;
        }
    }

    offset += s.offset;

    s.plants
        .iter()
        .enumerate()
        .fold(0, |a, (i, &p)| a + p as isize * (offset + i as isize))
}

mod parser {
    use super::State;
    use commons::lines;
    use nom::types::CompleteStr;
    use nom::{
        alt, char, count, do_parse, eol, many1, map, named, preceded, tag, terminated, value, IResult,
    };

    named!(plant<CompleteStr, u8>,
        alt!(
            value!(0, char!('.'))
            | value!(1, char!('#'))
        )
    );

    named!(state<CompleteStr, State>, map!(many1!(plant), State::new));

    named!(initial_state<CompleteStr, State>, preceded!(tag!("initial state: "), state));

    fn situation_code(plants: &[u8]) -> u8 {
        plants
            .iter()
            .rev()
            .fold((1, 0), |(i, a), p| (i << 1, a + i * p))
            .1
    }

    named!(rule<CompleteStr, (u8, u8)>,
        do_parse!(
            cond: count!(plant, 5) >>
                  tag!(" => ")     >>
            res:  plant            >>
            (situation_code(&cond), res)
        )
    );

    fn with_rules(mut state: State, rules: Vec<(u8, u8)>) -> State {
        for r in rules.into_iter() {
            state.add_rule(r);
        }
        state
    }

    #[rustfmt::skip]
    pub(super) fn input(i: CompleteStr) -> IResult<CompleteStr, State> {
        do_parse!(i,
            init:  terminated!(initial_state, eol) >>
                   eol                             >>
            rules: lines!(rule)                    >>
            (with_rules(init, rules))
        )
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn test_parse_initial_state() {
            assert_eq!(
                (0, "#..#".to_string()),
                initial_state(CompleteStr(&"initial state: #..#"))
                    .unwrap()
                    .1
                    .unparse()
            );
        }

        #[test]
        fn test_parse_state() {
            let s = state(CompleteStr(&"#....")).unwrap().1;
            assert_eq!(vec![1], crate::plants(&s.plants).0);
            assert_eq!(0, s.offset);
        }

        #[test]
        fn test_fix_padding() {
            assert_eq!(
                (0, "#".to_string()),
                state(CompleteStr(&"#")).unwrap().1.unparse()
            );
            assert_eq!(
                (7, "#".to_string()),
                state(CompleteStr(&".......#.......")).unwrap().1.unparse()
            );
            assert_eq!((1, "".to_string()), state(CompleteStr(&".")).unwrap().1.unparse());
        }

        #[test]
        fn test_parse_rule() {
            assert_eq!((0b10101, 1), rule(CompleteStr(&"#.#.# => #")).unwrap().1);
            assert_eq!((0b10000, 0), rule(CompleteStr(&"#.... => .")).unwrap().1);
        }
    }
}

fn read_input<'a>() -> Result<'a, State> {
    parse_input!(parser::input)
}

#[derive(Clone)]
struct State {
    plants: Vec<u8>,
    offset: isize,
    next_plants: Vec<u8>,
    rules: Vec<u8>,
}

impl State {
    fn new(plants: Vec<u8>) -> Self {
        State {
            plants,
            offset: 0,
            next_plants: vec![],
            rules: vec![0; 32],
        }
    }

    fn add_rule(&mut self, (situation, result): (u8, u8)) {
        assert!(situation < 32);
        assert!(result < 2);
        self.rules[situation as usize] = result;
    }

    fn ensure_trailing_zeroes(&mut self, n: usize) {
        let new_size = n + self.plants.iter().rposition(|&v| v == 1).unwrap_or(0);
        self.plants.resize(new_size, 0);
    }

    fn update(&mut self) {
        self.next_plants.clear();
        self.ensure_trailing_zeroes(5);
        let mut situation = 0;
        let (plants, offset) = dump(&self.plants);
        for p in plants {
            situation = (situation * 2 + p) & 0x1f;
            self.next_plants.push(self.rules[situation as usize]);
        }
        self.offset += offset - 2;
        std::mem::swap(&mut self.plants, &mut self.next_plants);
    }

    #[cfg(test)]
    fn unparse(&mut self) -> (isize, String) {
        self.ensure_trailing_zeroes(1);
        let (plants, offset) = dump(&self.plants);
        (
            self.offset + offset,
            plants.iter().map(|&p| if p == 1 { '#' } else { '.' }).collect(),
        )
    }
}

fn dump(plants: &[u8]) -> (&[u8], isize) {
    let offset = plants
        .iter()
        .position(|&v| v == 1)
        .unwrap_or_else(|| plants.len());
    (&plants[offset..], offset as isize)
}

fn plants(plants: &[u8]) -> (&[u8], isize) {
    let (plants, offset) = dump(plants);
    let r_offset = plants.iter().rposition(|&v| v == 1).unwrap_or(0);
    (&plants[..=r_offset], offset)
}

#[cfg(test)]
mod tests {
    use super::*;
    use indoc::indoc;
    use nom::types::CompleteStr;

    #[test]
    fn test_update() {
        let (_, mut s) = parser::input(CompleteStr(&indoc!(
            "
            initial state: #

            ...#. => #
            .#... => #
            "
        )))
        .unwrap();

        s.update();
        assert_eq!((-1, "#.#".to_string()), s.unparse());
        s.update();
        assert_eq!((-2, "#...#".to_string()), s.unparse());
        s.update();
        assert_eq!((-3, "#.#.#.#".to_string()), s.unparse());
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref STATE: State = read_input().unwrap();
    }

    #[test]
    fn test_20_iterations() {
        assert_eq!(1430, sum_after_n_gens(&mut STATE.clone(), 20));
    }

    #[test]
    fn test_50_000_000_000_iterations() {
        assert_eq!(
            1_150_000_000_457,
            sum_after_n_gens(&mut STATE.clone(), 50_000_000_000)
        );
    }
}
