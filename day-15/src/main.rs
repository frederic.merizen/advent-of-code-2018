use commons::Error::InvalidInput;
use commons::{input_iterator, Result};
use maplit::{btreemap, btreeset, hashmap};
use std::collections::{BTreeMap, HashMap};
use std::default::Default;
use CombatOutcome::*;
use Race::*;
use Square::*;

fn main() {
    let v: Vec<_> = input_iterator().unwrap().map(|l| l.unwrap()).collect();
    println!("Outcome {}", outcome(v.iter()).unwrap());
    println!("Fixed Outcome {}", fixed_outcome(v.iter()).unwrap());
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
enum Race {
    Elf,
    Goblin,
}

impl Race {
    fn target_race(self) -> Self {
        match self {
            Elf => Goblin,
            Goblin => Elf,
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd)]
struct Position {
    y: usize,
    x: usize,
}

impl Position {
    fn around(&self) -> Vec<Position> {
        let &Position { x, y } = self;
        vec![
            Position { x, y: y - 1 },
            Position { x: x - 1, y },
            Position { x: x + 1, y },
            Position { x, y: y + 1 },
        ]
    }

    fn dist(&self, other: &Position) -> usize {
        let Position { x, y } = self;
        let Position { x: ox, y: oy } = other;
        (if x > ox { x - ox } else { ox - x }) + (if y > oy { y - oy } else { oy - y })
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
struct Unit {
    race: Race,
    hp: u8,
}

#[derive(Clone, Debug, Eq, PartialEq)]
enum Square {
    Wall,
    Open {
        unit: Option<usize>,
        adjacent_units: HashMap<Race, u8>,
    },
}

impl Square {
    fn inc_adjacent(&mut self, race: Race) {
        if let Open { adjacent_units, .. } = self {
            *adjacent_units.entry(race).or_insert(0) += 1;
        }
    }

    fn dec_adjacent(&mut self, race: Race) {
        if let Open { adjacent_units, .. } = self {
            adjacent_units.entry(race).and_modify(|c| *c -= 1);
        }
    }

    fn set_unit(&mut self, u: usize) {
        if let Open { unit, .. } = self {
            *unit = Some(u);
        } else {
            panic!("Walls cannont bear units");
        }
    }

    fn unset_unit(&mut self) {
        if let Open { unit, .. } = self {
            *unit = None;
        } else {
            panic!("Walls cannont bear units");
        }
    }

    fn is_open(&self) -> bool {
        match self {
            Open { unit: None, .. } => true,
            _ => false,
        }
    }

    fn is_near(&self, race: Race) -> bool {
        if let Open { adjacent_units, .. } = self {
            if let Some(&n) = adjacent_units.get(&race) {
                return n > 0;
            }
            false
        } else {
            panic!("Walls cannont be ‘near’ enemies");
        }
    }
}

#[derive(Clone, Default)]
struct Field {
    squares: Vec<Square>,
    width: usize,
}

impl Field {
    fn square(&self, &Position { x, y }: &Position) -> &Square {
        &self.squares[x + y * self.width]
    }

    fn square_mut(&mut self, &Position { x, y }: &Position) -> &mut Square {
        &mut self.squares[x + y * self.width]
    }

    fn add_unit(&mut self, p: &Position, unit_no: usize, race: Race) {
        self.square_mut(p).set_unit(unit_no);
        for p in p.around() {
            self.square_mut(&p).inc_adjacent(race);
        }
    }

    fn remove_unit(&mut self, p: &Position, race: Race) {
        self.square_mut(p).unset_unit();
        for p in p.around() {
            self.square_mut(&p).dec_adjacent(race);
        }
    }

    fn find_nearest<F>(&self, start: &Position, goal: F) -> Option<Position>
    where
        F: Fn(&Position) -> bool,
    {
        let mut last = btreeset! {};
        let mut current = btreeset! { *start };

        while !current.is_empty() {
            let mut next = btreeset! {};
            for p in current.iter() {
                if goal(p) {
                    return Some(*p);
                }
                for p in p.around().into_iter().filter(|p| !last.contains(p)) {
                    if self.square(&p).is_open() {
                        next.insert(p);
                    }
                }
            }
            last = current;
            current = next;
        }

        None
    }
}

#[derive(Debug, Eq, PartialEq)]
enum CombatOutcome {
    FirstBlood(Race),
    WipedOut(Race),
    Undecided,
}

#[derive(Clone, Default)]
struct Combat {
    unit_count: HashMap<Race, usize>,
    units: Vec<Unit>,
    units_by_position: BTreeMap<Position, usize>,
    field: Field,
    ok_to_kill_elves: bool,
    elf_attack_power: u8,
    rounds_played: usize,
}

impl Combat {
    fn parse<'a, T>(ground: T) -> Result<'a, Combat>
    where
        T: IntoIterator,
        T::Item: AsRef<str>,
    {
        let mut width = None;
        let mut field: Field = Default::default();
        let mut units = vec![];
        let mut unit_count = hashmap! {};
        let mut units_by_position = btreemap! {};

        for (y, l) in ground.into_iter().enumerate() {
            let l = l.as_ref();
            if let Some(w) = width {
                if w != l.len() {
                    return Err(InvalidInput);
                }
            } else {
                field.width = l.len();
                width = Some(field.width);
            }

            for (x, c) in l.chars().enumerate() {
                field.squares.push(match c {
                    '#' => Wall,
                    '.' | 'E' | 'G' => Open {
                        unit: None,
                        adjacent_units: hashmap! {},
                    },
                    _ => {
                        return Err(InvalidInput);
                    }
                });
                if let Some(race) = match c {
                    'E' => Some(Elf),
                    'G' => Some(Goblin),
                    _ => None,
                } {
                    units_by_position.insert(Position { x, y }, units.len());
                    units.push(Unit { hp: 200, race });
                    *unit_count.entry(race).or_default() += 1;
                }
            }
        }

        for (pos, &unit_no) in units_by_position.iter() {
            field.add_unit(&pos, unit_no, units[unit_no].race);
        }

        Ok(Combat {
            field,
            unit_count,
            units,
            units_by_position,
            ok_to_kill_elves: true,
            elf_attack_power: 3,
            rounds_played: 0,
        })
    }

    #[cfg(test)]
    fn dump(&self) -> String {
        let mut res = String::new();
        let mut hp_res = vec![];

        for (i, s) in self.field.squares.iter().enumerate() {
            res.push(match s {
                Wall => '#',
                Open { unit: Some(unit), .. } => {
                    let unit = &self.units[*unit];
                    let c = match unit.race {
                        Elf => 'E',
                        Goblin => 'G',
                    };
                    hp_res.push(format!("{}({})", c, unit.hp));
                    c
                }
                Open { unit: None, .. } => '.',
            });

            if (i + 1) % self.field.width == 0 {
                if !hp_res.is_empty() {
                    res.push_str(&"   ");
                    res.push_str(&hp_res.join(", "));
                }
                hp_res.clear();
                res.push('\n');
            }
        }

        res
    }

    fn remove_unit(&mut self, pos: &Position, race: Race) {
        self.units_by_position.remove(pos);
        self.field.remove_unit(&pos, race);
    }

    fn place_unit(&mut self, pos: Position, unit_no: usize, race: Race) {
        self.units_by_position.insert(pos, unit_no);
        self.field.add_unit(&pos, unit_no, race);
    }

    fn move_towards_target(&mut self, pos: &Position, unit_no: usize) -> Option<Position> {
        let race = self.units[unit_no].race;
        let target_race = race.target_race();
        if let Some(target) = self.field.find_nearest(pos, |p| {
            let s = self.field.square(p);
            s.is_near(target_race) && s.is_open()
        }) {
            let next = self
                .field
                .find_nearest(&target, |p| (p.dist(pos) == 1) && self.field.square(p).is_open())
                .unwrap();
            self.remove_unit(pos, race);
            self.place_unit(next, unit_no, race);
            Some(next)
        } else {
            None
        }
    }

    fn attack_target(&mut self, pos: &Position, unit_no: usize) -> CombatOutcome {
        struct Target {
            hp: u8,
            unit_no: usize,
            pos: Position,
        }
        let race = self.units[unit_no].race;
        let target_race = race.target_race();
        let mut target: Option<Target> = None;

        for p in pos.around() {
            if let Open {
                unit: Some(unit_no), ..
            } = *self.field.square(&p)
            {
                let unit = &self.units[unit_no];
                if unit.race != target_race {
                    continue;
                }
                match &target {
                    Some(t) if t.hp <= unit.hp => (),
                    _ => {
                        target = Some(Target {
                            hp: unit.hp,
                            unit_no,
                            pos: p,
                        });
                    }
                }
            }
        }

        let Target { unit_no, pos, hp } = target.unwrap();
        let unit = &mut self.units[unit_no];

        let attack_power = match race {
            Elf => self.elf_attack_power,
            Goblin => 3,
        };

        if hp > attack_power {
            unit.hp -= attack_power;
        } else {
            self.unit_count.entry(target_race).and_modify(|c| *c -= 1);
            unit.hp = 0;
            self.remove_unit(&pos, target_race);
            if target_race == Elf && !self.ok_to_kill_elves {
                return FirstBlood(target_race);
            }
        }
        Undecided
    }

    fn play_turn(&mut self, pos: &Position, unit_no: usize) -> CombatOutcome {
        let target_race = self.units[unit_no].race.target_race();
        if self.unit_count[&target_race] == 0 {
            return WipedOut(target_race);
        }
        let mut is_near_target = self.field.square(pos).is_near(target_race);
        if is_near_target {
            return self.attack_target(pos, unit_no);
        } else if let Some(pos) = self.move_towards_target(pos, unit_no) {
            is_near_target = self.field.square(&pos).is_near(target_race);
            if is_near_target {
                return self.attack_target(&pos, unit_no);
            }
        }
        Undecided
    }

    fn play_round(&mut self) -> CombatOutcome {
        for (pos, unit_no) in self.units_by_position.clone().into_iter() {
            if self.units[unit_no].hp == 0 {
                continue;
            }
            let outcome = self.play_turn(&pos, unit_no);
            if outcome != Undecided {
                return outcome;
            }
        }
        self.rounds_played += 1;
        Undecided
    }

    fn fight_to_the_death(&mut self) -> CombatOutcome {
        loop {
            let outcome = self.play_round();
            if Undecided != outcome {
                return outcome;
            }
        }
    }

    fn total_hitpoints(&self) -> usize {
        self.units.iter().map(|u| u.hp as usize).sum()
    }
}

fn outcome<'a, T>(ground: T) -> Result<'a, usize>
where
    T: IntoIterator,
    T::Item: AsRef<str>,
{
    let mut c = Combat::parse(ground)?;
    c.fight_to_the_death();
    let hp = c.total_hitpoints();
    Ok(c.rounds_played * hp)
}

fn fixed_outcome<'a, T>(ground: T) -> Result<'a, usize>
where
    T: IntoIterator,
    T::Item: AsRef<str>,
{
    let mut c0 = Combat::parse(ground)?;
    c0.ok_to_kill_elves = false;
    let c0 = c0;
    let mut attack_power = 3;
    let mut hits_to_kill = (200 + attack_power - 1) / attack_power;
    loop {
        if attack_power < hits_to_kill {
            attack_power += 1;
            hits_to_kill = (200 + attack_power - 1) / attack_power;
        } else {
            hits_to_kill -= 1;
            attack_power = (200 + (hits_to_kill as usize) - 1) / (hits_to_kill as usize);
        }
        let mut c = c0.clone();
        c.elf_attack_power = attack_power as u8;
        if WipedOut(Goblin) == c.fight_to_the_death() {
            return Ok(c.rounds_played * c.total_hitpoints());
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use indoc::indoc;

    #[test]
    fn test_target_race() {
        assert_eq!(Elf, Goblin.target_race());
        assert_eq!(Goblin, Elf.target_race());
    }

    fn combat(ground: Vec<&str>) -> Combat {
        Combat::parse(ground.into_iter()).unwrap()
    }

    fn square(c: &Combat, x: usize, y: usize) -> &Square {
        c.field.square(&Position { x, y })
    }

    #[test]
    fn test_parse_combat() {
        let c = combat(vec![
            "#######", //
            "#E..G.#", //
            "#...#.#", //
            "#.G.#G#", //
            "#######",
        ]);

        assert_eq!(1, c.unit_count[&Elf]);
        assert_eq!(3, c.unit_count[&Goblin]);

        assert_eq!(Unit { race: Elf, hp: 200 }, c.units[0]);
        assert_eq!(
            Unit {
                race: Goblin,
                hp: 200
            },
            c.units[1]
        );
        assert_eq!(
            Unit {
                race: Goblin,
                hp: 200
            },
            c.units[2]
        );
        assert_eq!(
            Unit {
                race: Goblin,
                hp: 200
            },
            c.units[3]
        );

        assert_eq!(&Wall, square(&c, 0, 0));
        assert_eq!(&Wall, square(&c, 4, 2));
        assert_eq!(
            &Open {
                adjacent_units: hashmap! {},
                unit: None
            },
            square(&c, 3, 2)
        );
        assert_eq!(
            &Open {
                adjacent_units: hashmap! {},
                unit: Some(0)
            },
            square(&c, 1, 1)
        );
        assert_eq!(
            &Open {
                adjacent_units: hashmap! { Elf => 1},
                unit: None
            },
            square(&c, 1, 2)
        );
    }

    #[test]
    fn test_dump() {
        let c = combat(vec![
            "#######", //
            "#E..G.#", //
            "#...#.#", //
            "#.G.#G#", //
            "#######",
        ]);

        assert_eq!(
            indoc!(
                "
                #######
                #E..G.#   E(200), G(200)
                #...#.#
                #.G.#G#   G(200), G(200)
                #######
                "
            ),
            c.dump()
        );
    }

    #[test]
    fn test_position_order() {
        assert!(Position { x: 0, y: 0 } < Position { x: 1, y: 0 });
        assert!(Position { x: 0, y: 0 } < Position { x: 0, y: 1 });
        assert!(Position { x: 1, y: 0 } < Position { x: 0, y: 1 });
        assert!(Position { x: 0, y: 1 } < Position { x: 1, y: 1 });
    }

    #[test]
    fn test_manhattan_distance() {
        let a = Position { x: 15, y: 17 };
        let b = Position { x: 16, y: 17 };
        let c = Position { x: 10, y: 17 };
        assert_eq!(0, a.dist(&a));
        assert_eq!(1, a.dist(&b));
        assert_eq!(1, b.dist(&a));
        assert_eq!(5, a.dist(&c));
        assert_eq!(5, c.dist(&a));
    }

    #[test]
    fn test_find_nearest_is_restricted_by_walls_and_units() {
        let c = combat(vec![
            "#######", //
            "#.....#", //
            "#.###.#", //
            "#...E.#", //
            "#######",
        ]);

        let n = c
            .field
            .find_nearest(&Position { x: 3, y: 3 }, |&Position { x, y }| {
                x == 5 && (y == 3 || y == 1)
            })
            .unwrap();

        assert_eq!(Position { x: 5, y: 1 }, n);
    }

    #[test]
    fn test_find_nearest_prefers_higher_to_lower() {
        let c = combat(vec![
            "#####", //
            "#...#", //
            "#...#", //
            "#...#", //
            "#####",
        ]);

        let n = c
            .field
            .find_nearest(&Position { x: 2, y: 2 }, |&Position { x, y }| {
                (if x >= 2 { x - 2 } else { 2 - x }) + (if y >= 2 { y - 2 } else { 2 - y }) == 1
            })
            .unwrap();

        assert_eq!(Position { x: 2, y: 1 }, n);
    }

    #[test]
    fn test_find_nearest_prefers_left_to_right_when_tied_for_height() {
        let c = combat(vec![
            "#####", //
            "#...#", //
            "#####",
        ]);

        let n = c
            .field
            .find_nearest(&Position { x: 2, y: 1 }, |&Position { x, .. }| {
                (if x >= 2 { x - 2 } else { 2 - x }) == 1
            })
            .unwrap();

        assert_eq!(Position { x: 1, y: 1 }, n);
    }

    #[test]
    fn test_is_square_near_target_race() {
        let c = combat(vec![
            "###########", //
            "#.E.G.GG..#", //
            "###########", //
        ]);

        assert!(square(&c, 1, 1).is_near(Elf));
        assert!(!square(&c, 1, 1).is_near(Goblin));

        assert!(square(&c, 3, 1).is_near(Elf));
        assert!(square(&c, 3, 1).is_near(Goblin));

        assert!(!square(&c, 5, 1).is_near(Elf));
        assert!(square(&c, 5, 1).is_near(Goblin));

        assert!(!square(&c, 6, 1).is_near(Elf));
        assert!(square(&c, 6, 1).is_near(Goblin));

        let s = square(&c, 0, 0);
        let result = std::panic::catch_unwind(|| s.is_near(Elf));
        assert!(result.is_err());
    }

    #[test]
    fn test_move_toward_target() {
        let mut c = combat(vec![
            "#####", //
            "#E..#", //
            "#...#", //
            "#..G#", //
            "#####", //
        ]);

        let (&pos, &elf) = c.units_by_position.iter().next().unwrap();
        c.move_towards_target(&pos, elf);

        assert_eq!(
            indoc!(
                "
                #####
                #.E.#   E(200)
                #...#
                #..G#   G(200)
                #####
                "
            ),
            c.dump()
        );

        let (&new_pos, &still_elf) = c.units_by_position.iter().next().unwrap();
        assert_eq!(elf, still_elf);
        assert_eq!(Position { x: 2, y: 1 }, new_pos);
        assert!(match square(&c, 1, 1) {
            Open { unit, .. } => unit,
            _ => panic!("Square should be open"),
        }
        .is_none());
        assert_eq!(
            0,
            match square(&c, 1, 2) {
                Open { adjacent_units, .. } => adjacent_units[&Elf],
                _ => panic!("Square should be open"),
            }
        );
        assert_eq!(
            1,
            match square(&c, 1, 1) {
                Open { adjacent_units, .. } => adjacent_units[&Elf],
                _ => panic!("Square should be open"),
            }
        );
    }

    #[test]
    fn test_attack_target() {
        let mut c = combat(vec![
            "#####", //
            "#.G.#", //
            "#EEG#", //
            "#.G.#", //
            "#####", //
        ]);

        c.units[1].hp = 137;
        c.units[3].hp = 150;
        c.units[4].hp = 150;
        c.attack_target(&Position { x: 2, y: 2 }, 2);

        assert_eq!(
            indoc!(
                "
                #####
                #.G.#   G(200)
                #EEG#   E(137), E(200), G(147)
                #.G.#   G(150)
                #####
                "
            ),
            c.dump()
        );
    }

    #[test]
    fn test_remove_killed_target() {
        let mut c = combat(vec![
            "####", //
            "#EG#", //
            "####", //
        ]);

        c.units[1].hp = 2;
        let (&pos, &elf) = c.units_by_position.iter().next().unwrap();
        c.attack_target(&pos, elf);

        assert_eq!(
            indoc!(
                "
                ####
                #E.#   E(200)
                ####
                "
            ),
            c.dump()
        );

        assert_eq!(0, c.unit_count[&Goblin]);
        assert_eq!(0, c.units[1].hp);
        assert_eq!(btreemap! { Position{x: 1, y: 1} => elf}, c.units_by_position);
        assert!(match square(&c, 2, 1) {
            Open { unit, .. } => unit,
            _ => panic!("Square should be open"),
        }
        .is_none());
        assert_eq!(
            0,
            match square(&c, 1, 1) {
                Open { adjacent_units, .. } => adjacent_units[&Goblin],
                _ => panic!("Square should be open"),
            }
        );
    }

    #[test]
    fn test_play_turn() {
        let mut c = combat(vec![
            "#####", //
            "#E.G#", //
            "#####", //
        ]);

        c.units[1].hp = 3;
        let (&pos, &elf) = c.units_by_position.iter().next().unwrap();
        let has_won = c.play_turn(&pos, elf);

        assert_eq!(
            indoc!(
                "
                #####
                #.E.#   E(200)
                #####
                "
            ),
            c.dump()
        );

        assert_eq!(Undecided, has_won);
        assert_eq!(0, c.unit_count[&Goblin]);
        assert_eq!(0, c.units[1].hp);
        assert_eq!(btreemap! { Position{x: 2, y: 1} => elf}, c.units_by_position);

        let has_won = c.play_turn(&pos, elf);
        assert_eq!(WipedOut(Goblin), has_won);
    }

    #[test]
    fn test_play_round() {
        let mut c = combat(vec![
            "#######", //
            "#.G...#", //
            "#...EG#", //
            "#.#.#G#", //
            "#..G#E#", //
            "#.....#", //
            "#######", //
        ]);

        let has_won = c.play_round();
        assert_eq!(Undecided, has_won);

        assert_eq!(
            indoc!(
                "
                #######
                #..G..#   G(200)
                #...EG#   E(197), G(197)
                #.#G#G#   G(200), G(197)
                #...#E#   E(197)
                #.....#
                #######
                "
            ),
            c.dump()
        );

        for _ in 0..25 {
            let has_won = c.play_round();
            assert_eq!(Undecided, has_won);
        }

        assert_eq!(
            indoc!(
                "
                #######
                #G....#   G(200)
                #.G...#   G(131)
                #.#.#G#   G(122)
                #...#E#   E(122)
                #..G..#   G(200)
                #######
                "
            ),
            c.dump()
        );

        let has_won = c.play_round();
        assert_eq!(Undecided, has_won);

        assert_eq!(
            indoc!(
                "
                #######
                #G....#   G(200)
                #.G...#   G(131)
                #.#.#G#   G(119)
                #...#E#   E(119)
                #...G.#   G(200)
                #######
                "
            ),
            c.dump()
        );

        for _ in 0..20 {
            let has_won = c.play_round();
            assert_eq!(Undecided, has_won);
        }

        let has_won = c.play_round();
        assert_eq!(WipedOut(Elf), has_won);
    }

    #[test]
    fn test_fight_to_the_death() {
        let mut c = combat(vec![
            "#######", //
            "#.G...#", //
            "#...EG#", //
            "#.#.#G#", //
            "#..G#E#", //
            "#.....#", //
            "#######", //
        ]);

        let o = c.fight_to_the_death();

        assert_eq!(
            indoc!(
                "
                #######
                #G....#   G(200)
                #.G...#   G(131)
                #.#.#G#   G(59)
                #...#.#
                #....G#   G(200)
                #######
                "
            ),
            c.dump()
        );

        assert_eq!(WipedOut(Elf), o);
        assert_eq!(47, c.rounds_played);
        assert_eq!(590, c.total_hitpoints());
    }

    #[test]
    fn test_another_fight_to_the_death() {
        let mut c = combat(vec![
            "#######", //
            "#G..#E#", //
            "#E#E.E#", //
            "#G.##.#", //
            "#...#E#", //
            "#...E.#", //
            "#######", //
        ]);

        let o = c.fight_to_the_death();

        assert_eq!(
            indoc!(
                "
                #######
                #...#E#   E(200)
                #E#...#   E(197)
                #.E##.#   E(185)
                #E..#E#   E(200), E(200)
                #.....#
                #######
                "
            ),
            c.dump()
        );

        assert_eq!(982, c.total_hitpoints());
        assert_eq!(WipedOut(Goblin), o);
        assert_eq!(37, c.rounds_played);
    }

    #[test]
    fn test_outcome() {
        assert_eq!(
            27730,
            outcome(vec![
                "#######", //
                "#.G...#", //
                "#...EG#", //
                "#.#.#G#", //
                "#..G#E#", //
                "#.....#", //
                "#######", //
            ])
            .unwrap()
        );
        assert_eq!(
            36334,
            outcome(vec![
                "#######", //
                "#G..#E#", //
                "#E#E.E#", //
                "#G.##.#", //
                "#...#E#", //
                "#...E.#", //
                "#######", //
            ])
            .unwrap()
        );
    }

    #[test]
    fn test_halt_on_first_elf_kill() {
        let mut c = combat(vec![
            "#######", //
            "#GEG.E#", //
            "#######", //
        ]);
        c.ok_to_kill_elves = false;

        c.fight_to_the_death();

        assert_eq!(
            indoc!(
                "
                #######
                #G.GE.#   G(101), G(101), E(200)
                #######
                "
            ),
            c.dump()
        );
    }

    #[test]
    fn test_boost_elves() {
        let mut c = combat(vec![
            "#######", //
            "#GEG.E#", //
            "#######", //
        ]);
        c.elf_attack_power = 50;

        c.fight_to_the_death();

        assert_eq!(
            indoc!(
                "
                #######
                #.E.E.#   E(176), E(200)
                #######
                "
            ),
            c.dump()
        );
    }

    #[test]
    fn test_fixed_outcome() {
        assert_eq!(
            4988,
            fixed_outcome(vec![
                "#######", //
                "#.G...#", //
                "#...EG#", //
                "#.#.#G#", //
                "#..G#E#", //
                "#.....#", //
                "#######", //
            ])
            .unwrap()
        );
        assert_eq!(
            31284,
            fixed_outcome(vec![
                "#######", //
                "#E..EG#", //
                "#.#G.E#", //
                "#E.##E#", //
                "#G..#.#", //
                "#..E#.#", //
                "#######", //
            ])
            .unwrap()
        );
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref IN: Vec<String> = input_iterator().unwrap().collect::<Result<_>>().unwrap();
    }

    #[test]
    fn test_unbiased_outcome() {
        assert_eq!(195_811, outcome(IN.iter()).unwrap());
    }

    #[test]
    fn test_fixed_outcome() {
        assert_eq!(69_867, fixed_outcome(IN.iter()).unwrap());
    }
}
