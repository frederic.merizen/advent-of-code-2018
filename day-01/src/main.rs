use commons::parser::int;
use commons::{lines, parse_input, Result};
use std::collections::HashSet;

fn read_input<'a>() -> Result<'a, Vec<i64>> {
    parse_input!(lines!(int))
}

fn sum<'a, T>(ls: T) -> i64
where
    T: IntoIterator<Item = &'a i64>,
{
    ls.into_iter().sum()
}

fn first_repetition<'a, T>(ls: T) -> i64
where
    T: IntoIterator<Item = &'a i64>,
    T::IntoIter: std::clone::Clone,
{
    ls.into_iter()
        .cycle()
        .scan(0, |state, &line| {
            *state += line;
            Some(*state)
        })
        .scan((HashSet::new(), false), |(seen, found), next| {
            if *found {
                None
            } else {
                *found = seen.contains(&next);
                seen.insert(next);
                Some(next)
            }
        })
        .last()
        .unwrap()
}

fn main() {
    let lines = read_input().unwrap();

    println!("Total: {}", sum(&lines));
    println!("First repetition: {}", first_repetition(&lines));
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref LINES: Vec<i64> = read_input().unwrap();
    }

    #[test]
    fn test_sum() {
        assert_eq!(459, sum(&*LINES));
    }

    #[test]
    fn test_first_repetition() {
        assert_eq!(65474, first_repetition(&*LINES));
    }
}
