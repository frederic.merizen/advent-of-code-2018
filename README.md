# Advent of code 2018, solutions in Rust

## Puzzles
- [Summing integers](day-01/src/main.rs)
- [Comparing box serial numbers](day-02/src/main.rs)
- [Overlapping pieces of fabric](day-03/src/main.rs)
- [Sleepy guards](day-04/src/main.rs)
- [Reducing polymers](day-05/src/main.rs)
- [Areas controlled by points](day-06/src/main.rs)
- [Assembly sequences](day-07/src/main.rs)
- [Serial number trees](day-08/src/main.rs)
- [Marble game](day-09/src/main.rs)
- [Message in the stars](day-10/src/main.rs)
- [Grid of fuel cells](day-11/src/main.rs)
- [Plant growth cellular automaton](day-12/src/main.rs)

## To do

### Day 1
- Add unit tests
- Instead of brute forcing through the cycle, use modular arithmetic to get straight to the answer

### Day 2
- Add unit tests

### Day 3
- Add unit tests

### Day 4
- Add unit tests

### Day 5
- Add unit tests

### Day 6
- Use properties of Mahattan distance to avoid recomputing and summing distances

### Day 10
- Add unit tests
- Add integration tests
- Use inversion of area variation as stopping criterion

### Day 11
- Use integral image

### Day 18
- Find a way to make the CycleDetector work with references rather than clones, and get
  rid of `fast_skip`
