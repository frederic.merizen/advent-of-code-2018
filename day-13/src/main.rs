use commons::Error::InvalidInput;
use commons::{input_iterator, OrInvalid, Result};
use maplit::{btreemap, hashset};
use std::cmp::Ordering;
use std::collections::BTreeMap;
use std::mem;
use Turn::*;

#[derive(Copy, Clone, Debug, Hash, PartialEq, Eq)]
struct Position {
    x: i32,
    y: i32,
}

impl Ord for Position {
    fn cmp(&self, rhs: &Self) -> Ordering {
        let mut res = self.y.cmp(&rhs.y);
        if res == Ordering::Equal {
            res = self.x.cmp(&rhs.x);
        }
        res
    }
}

impl PartialOrd for Position {
    fn partial_cmp(&self, rhs: &Self) -> Option<Ordering> {
        Some(self.cmp(rhs))
    }
}

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
struct Direction {
    x: i32,
    y: i32,
}

#[derive(Debug, Copy, Clone, Eq, Hash, PartialEq)]
enum Turn {
    Left,
    Straight,
    Right,
}

impl Turn {
    fn from_u8(u: u8) -> Option<Self> {
        Some(match u {
            0 => Left,
            1 => Straight,
            2 => Right,
            _ => return None,
        })
    }
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
struct Train {
    pos: Position,
    dir: Direction,
    turn: Turn,
}

impl Train {
    fn parse<'a>(x: i32, y: i32, tracks: char) -> Result<'a, Train> {
        let pos = Position { x, y };
        let dir = match tracks {
            '>' => Direction { x: 1, y: 0 },
            '<' => Direction { x: -1, y: 0 },
            '^' => Direction { x: 0, y: -1 },
            'v' => Direction { x: 0, y: 1 },
            _ => return Err(InvalidInput),
        };
        Ok(Train { pos, dir, turn: Left })
    }

    #[cfg(test)]
    fn dump(&self) -> char {
        match self.dir {
            Direction { x: -1, y: 0 } => '<',
            Direction { x: 1, y: 0 } => '>',
            Direction { x: 0, y: -1 } => '^',
            Direction { x: 0, y: 1 } => 'v',
            _ => panic!("Invalid direction"),
        }
    }

    fn step<'a, 'b>(&'a mut self, tracks: char) -> Result<'b, ()> {
        self.turn(tracks)?;
        self.pos.x += self.dir.x;
        self.pos.y += self.dir.y;
        Ok(())
    }

    fn turn<'a, 'b>(&'a mut self, tracks: char) -> Result<'b, ()> {
        match tracks {
            '-' | '|' | '>' | '<' | 'v' | '^' => {}
            '/' => {
                let dir_x = self.dir.x;
                self.dir.x = -self.dir.y;
                self.dir.y = -dir_x;
            }
            '\\' => {
                mem::swap(&mut self.dir.x, &mut self.dir.y);
            }
            '+' => {
                match self.turn {
                    Left => {
                        let dir_x = self.dir.x;
                        self.dir.x = self.dir.y;
                        self.dir.y = -dir_x;
                    }
                    Straight => {}
                    Right => {
                        let dir_x = self.dir.x;
                        self.dir.x = -self.dir.y;
                        self.dir.y = dir_x;
                    }
                }
                self.turn = Turn::from_u8((self.turn as u8 + 1) % 3).unwrap();
            }
            _ => return Err(InvalidInput),
        }
        Ok(())
    }
}

#[derive(Clone)]
struct Field {
    trains: Vec<Train>,
    tracks: Vec<Vec<char>>,
    live_trains: BTreeMap<Position, usize>,
    collisions: Vec<Position>,
}

impl Field {
    fn new<S>(tracks: &[S]) -> Self
    where
        S: AsRef<str>,
    {
        let mut live_trains = btreemap! {};
        let mut trains = vec![];
        let mut tracks: Vec<Vec<_>> = tracks.iter().map(|l| l.as_ref().chars().collect()).collect();
        for (y, l) in tracks.iter_mut().enumerate() {
            for (x, track) in l.iter_mut().enumerate() {
                if let Ok(train) = Train::parse(x as i32, y as i32, *track) {
                    live_trains.insert(train.pos, trains.len());
                    trains.push(train);
                    *track = match *track {
                        '<' | '>' => '-',
                        '^' | 'v' => '|',
                        c => panic!("Unexpecet char {}", c),
                    }
                }
            }
        }
        Field {
            tracks,
            trains,
            live_trains,
            collisions: vec![],
        }
    }

    fn tick<'a, 'b>(&'a mut self) -> Result<'b, ()> {
        let mut crashed = hashset! {};
        let old_trains = self.live_trains.clone();
        for (p, t) in old_trains.into_iter() {
            if crashed.remove(&t) {
                continue;
            }
            self.live_trains.remove(&p);
            let train = &mut self.trains[t];
            train.step(self.tracks[p.y as usize][p.x as usize])?;
            if let Some(t2) = self.live_trains.remove(&train.pos) {
                crashed.insert(t2);
                self.collisions.push(train.pos);
            } else {
                self.live_trains.insert(train.pos, t);
            }
        }
        Ok(())
    }

    #[cfg(test)]
    fn dump(&self) -> Vec<String> {
        let mut res = self.tracks.clone();
        for (&Position { x, y }, &t) in self.live_trains.iter() {
            res[y as usize][x as usize] = self.trains[t].dump();
        }
        res.into_iter().map(|v| v.into_iter().collect()).collect()
    }
}

fn first_collision(f: &mut Field) -> Result<Position> {
    while f.collisions.is_empty() {
        f.tick()?;
    }
    Ok(*f.collisions.first().unwrap())
}

fn last_standing(f: &mut Field) -> Result<Position> {
    while f.live_trains.len() > 1 {
        f.tick()?;
    }
    f.live_trains.keys().next().cloned().or_invalid()
}

fn read_input<'a>() -> Result<'a, Field> {
    let input: Result<Vec<_>> = input_iterator()?.collect();
    Ok(Field::new(&input?))
}

fn main() {
    let mut f = read_input().unwrap();
    let Position { x, y } = first_collision(&mut f).unwrap();
    println!("First collision: {},{}", x, y);
    let Position { x, y } = last_standing(&mut f).unwrap();
    println!("Last stanging: {},{}", x, y);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_field() {
        let f = Field::new(&[
            r"/<\", //
            r"| |", //
            r"\-/",
        ]);

        assert_eq!(
            vec![Train {
                pos: Position { x: 1, y: 0 },
                dir: Direction { x: -1, y: 0 },
                turn: Left
            }],
            f.trains
        );
    }

    #[test]
    fn test_tick() {
        let mut f = Field::new(&[
            r"/<\", //
            r"| |", //
            r"\-/",
        ]);

        f.tick().unwrap();

        assert_eq!(
            vec![
                r"<-\", //
                r"| |", //
                r"\-/",
            ],
            f.dump()
        );

        f.tick().unwrap();

        assert_eq!(
            vec![
                r"/-\", //
                r"v |", //
                r"\-/",
            ],
            f.dump()
        );
    }

    #[test]
    fn test_collisions() {
        let mut f = Field::new(&[">-<"]);
        f.tick().unwrap();
        assert_eq!(vec![Position { x: 1, y: 0 }], f.collisions);
    }

    #[test]
    fn test_first_collision() {
        let mut f = Field::new(&[">-<>-<"]);
        assert_eq!(Position { x: 1, y: 0 }, first_collision(&mut f).unwrap());
    }

    #[test]
    fn test_last_standing() {
        let mut f = Field::new(&[">-<>--<>-----"]);
        assert_eq!(Position { x: 9, y: 0 }, last_standing(&mut f).unwrap());
    }

    fn step(t: &mut Train, step: char) {
        t.step(step).unwrap();
    }

    fn assert_train(t: &Train, pos: Position, dir: Direction, turn: Turn) {
        assert_eq!(pos, t.pos);
        assert_eq!(dir, t.dir);
        assert_eq!(turn, t.turn);
    }

    #[test]
    fn test_step() {
        let mut t = Train::parse(0, 0, '>').unwrap();
        assert_train(&t, Position { x: 0, y: 0 }, Direction { x: 1, y: 0 }, Left);

        step(&mut t, '>');
        assert_train(&t, Position { x: 1, y: 0 }, Direction { x: 1, y: 0 }, Left);

        step(&mut t, '-');
        assert_train(&t, Position { x: 2, y: 0 }, Direction { x: 1, y: 0 }, Left);

        step(&mut t, '\\');
        assert_train(&t, Position { x: 2, y: 1 }, Direction { x: 0, y: 1 }, Left);

        step(&mut t, '|');
        assert_train(&t, Position { x: 2, y: 2 }, Direction { x: 0, y: 1 }, Left);

        step(&mut t, '+');
        assert_train(&t, Position { x: 3, y: 2 }, Direction { x: 1, y: 0 }, Straight);

        step(&mut t, '-');
        assert_train(&t, Position { x: 4, y: 2 }, Direction { x: 1, y: 0 }, Straight);

        step(&mut t, '+');
        assert_train(&t, Position { x: 5, y: 2 }, Direction { x: 1, y: 0 }, Right);

        step(&mut t, '/');
        assert_train(&t, Position { x: 5, y: 1 }, Direction { x: 0, y: -1 }, Right);

        step(&mut t, '^');
        assert_train(&t, Position { x: 5, y: 0 }, Direction { x: 0, y: -1 }, Right);

        step(&mut t, '/');
        assert_train(&t, Position { x: 6, y: 0 }, Direction { x: 1, y: 0 }, Right);

        step(&mut t, '-');
        assert_train(&t, Position { x: 7, y: 0 }, Direction { x: 1, y: 0 }, Right);

        step(&mut t, '+');
        assert_train(&t, Position { x: 7, y: 1 }, Direction { x: 0, y: 1 }, Left);
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref FIELD: Field = read_input().unwrap();
    }

    #[test]
    fn test_first_crash() {
        assert_eq!(
            Position { x: 117, y: 62 },
            first_collision(&mut FIELD.clone()).unwrap()
        );
    }

    #[test]
    fn test_last_standing() {
        assert_eq!(
            Position { x: 69, y: 67 },
            last_standing(&mut FIELD.clone()).unwrap()
        );
    }
}
