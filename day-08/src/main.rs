use commons::{parse_input, Result};

mod parser {
    use super::Node;
    use commons::parser::uint;
    use nom::types::CompleteStr;
    use nom::{char, count, do_parse, eol, named, preceded, terminated, IResult};

    named!(node<CompleteStr, Node>,
        do_parse!(
            n_children: uint                                            >>
                        char!(' ')                                      >>
            n_meta:     uint                                            >>
            children:   count!(preceded!(char!(' '), node), n_children) >>
            meta:       count!(preceded!(char!(' '), uint), n_meta)     >>
            (Node { children, meta })
        )
    );

    pub(super) fn input(i: CompleteStr) -> IResult<CompleteStr, Node> {
        terminated!(i, node, eol)
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn test_parse_tree() {
            assert_eq!(
                Node {
                    children: vec![
                        Node {
                            children: vec![],
                            meta: vec![10, 11, 12],
                        },
                        Node {
                            children: vec![Node {
                                children: vec![],
                                meta: vec![99],
                            }],
                            meta: vec![2],
                        },
                    ],
                    meta: vec![1, 1, 2]
                },
                node(CompleteStr(&"2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2"))
                    .unwrap()
                    .1
            )
        }
    }
}

fn read_input<'a>() -> Result<'a, Node> {
    parse_input!(parser::input)
}

#[derive(Debug, Eq, PartialEq)]
struct Node {
    children: Vec<Node>,
    meta: Vec<u32>,
}

fn sum_meta(n: &Node) -> u32 {
    n.meta.iter().sum::<u32>() + n.children.iter().map(sum_meta).sum::<u32>()
}

fn node_value(n: &Node) -> u32 {
    if n.children.is_empty() {
        n.meta.iter().sum()
    } else {
        n.meta
            .iter()
            .filter_map(|i| {
                if *i > 0 {
                    Some(node_value(n.children.get((*i - 1) as usize)?))
                } else {
                    None
                }
            })
            .sum()
    }
}

fn main() {
    let node = read_input().unwrap();
    println!("Part 1: {}", sum_meta(&node));
    println!("Part 2: {}", node_value(&node));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sum_meta() {
        assert_eq!(
            138,
            sum_meta(&Node {
                children: vec![
                    Node {
                        children: vec![],
                        meta: vec![10, 11, 12],
                    },
                    Node {
                        children: vec![Node {
                            children: vec![],
                            meta: vec![99],
                        }],
                        meta: vec![2],
                    },
                ],
                meta: vec![1, 1, 2]
            })
        );
    }

    #[test]
    fn test_node_value() {
        assert_eq!(
            0,
            node_value(&Node {
                children: vec![Node {
                    children: vec![],
                    meta: vec![99],
                }],
                meta: vec![2],
            })
        );
        assert_eq!(
            66,
            node_value(&Node {
                children: vec![
                    Node {
                        children: vec![],
                        meta: vec![10, 11, 12],
                    },
                    Node {
                        children: vec![Node {
                            children: vec![],
                            meta: vec![99],
                        }],
                        meta: vec![2],
                    },
                ],
                meta: vec![1, 1, 2]
            })
        );
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref NODE: Node = read_input().unwrap();
    }

    #[test]
    fn test_sum_meta() {
        assert_eq!(45865, sum_meta(&*NODE));
    }

    #[test]
    fn test_node_value() {
        assert_eq!(22608, node_value(&*NODE));
    }
}
