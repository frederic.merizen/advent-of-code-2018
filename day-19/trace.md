| Input       | Step 1               | Step 2              | Step 3                                           |
|-------------|----------------------|---------------------|--------------------------------------------------|
| #ip 1       |                      |                     |                                                  |
|             |                      |   int a = 0;        | let mut a = 0;                                   |
| addi 1 16 1 | L0:  GOTO L17        |   int b = init();   | let b = if part\_2 { 970 } else { 1\_055\_170 }; |
| seti 1 1 3  | L1:  R3 = 1          |   int c = 1;        | for c in 1..= b {                                |
| seti 1 9 5  | L2:  R5 = 1          | outer:              |                                                  |
|             |                      |   int d = 1;        |   for d in 1..= b {                              |
| mulr 3 5 2  | L3:  R2 = R3 * R5    |                     |                                                  |
| eqrr 2 4 2  |      R2 = (R2 == R4) | inner:              |                                                  |
| addr 2 1 1  |      IF R2 GOTO L7   |   if (b == c * d) { |     if b == c * d {                              |
| addi 1 1 1  |      GOTO L8         |     a += c;         |                                                  |
| addr 3 0 0  | L7:  R0 += R3        |   }                 |       a += c;                                    |
| addi 5 1 5  | L8:  R5++            |   d++;              |                                                  |
| gtrr 5 4 2  |      R2 = R5 > R4    |   if (d <= b) {     |                                                  |
| addr 1 2 1  |      IF R2 GOTO L12  |     goto inner;     |   }                                              |
| seti 2 6 1  |      GOTO L3         |   }                 |                                                  |
| addi 3 1 3  | L12: R3++            |   b++;              |                                                  |
| gtrr 3 4 2  |      R2 = R3 > R4    |   if (c <= b) {     |                                                  |
| addr 2 1 1  |      IF R2 GOTO L16  |     goto outer;     | }                                                |
| seti 1 6 1  |      GOTO L2         |   }                 |                                                  |
| mulr 1 1 1  | L16: HALT            |   return a;         | a                                                |
|             |                      | int init() {        |                                                  |
| addi 4 2 4  | L17: R4 += 2         |   int b = 2;        |                                                  |
| mulr 4 4 4  |      R4 *= R4        |   b = 4;            |                                                  |
| mulr 1 4 4  |      R4 *= 19        |   b = 76;           |                                                  |
| muli 4 11 4 |      R4 *= 11        |   b = 836;          |                                                  |
| addi 2 6 2  |      R2 += 6         |   int e = 6;        |                                                  |
| mulr 2 1 2  |      R2 *= 22        |   e = 132;          |                                                  |
| addi 2 2 2  |      R2 += 2         |   e = 134;          |                                                  |
| addr 4 2 4  |      R4 += R2        |   b = 970;          |                                                  |
| addr 1 0 1  |      if R0 GOTO L27  |   if (part_2) {     |                                                  |
| seti 0 3 1  |      GOTO L1         |                     |                                                  |
| setr 1 4 2  | L27: R2 = 27         |     e = 27;         |                                                  |
| mulr 2 1 2  |      R2 *= 28        |     e = 756;        |                                                  |
| addr 1 2 2  |      R2 += 29        |     e = 785;        |                                                  |
| mulr 1 2 2  |      R2 *= 30        |     e = 23550;      |                                                  |
| muli 2 14 2 |      R2 *= 14        |     e = 329700;     |                                                  |
| mulr 2 1 2  |      R2 *= 32        |     e = 10550400;   |                                                  |
| addr 4 2 4  |      R4 += R2        |     b = 1055170;    |                                                  |
| seti 0 0 0  |      R0 = 0          |   }                 |                                                  |
| seti 0 4 1  |      GOTO L1         |   return b;         |                                                  |
|             |                      | }                   |                                                  |
