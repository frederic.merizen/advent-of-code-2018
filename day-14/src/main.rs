use commons::parse_input;

struct State {
    recipes: Vec<usize>,
    elf1: usize,
    elf2: usize,
}

impl State {
    fn new() -> Self {
        State {
            recipes: vec![3, 7],
            elf1: 0,
            elf2: 1,
        }
    }

    fn combine(&mut self) {
        let t = self.recipes[self.elf1] + self.recipes[self.elf2];
        let s1 = t / 10;
        let s2 = t % 10;
        if s1 > 0 {
            self.recipes.push(s1);
        }
        self.recipes.push(s2);
        self.elf1 = (self.elf1 + 1 + self.recipes[self.elf1]) % self.recipes.len();
        self.elf2 = (self.elf2 + 1 + self.recipes[self.elf2]) % self.recipes.len();
    }

    fn ten_scores_after(&mut self, n: usize) -> &[usize] {
        while self.recipes.len() < n + 10 {
            self.combine();
        }
        &self.recipes[n..(n + 10)]
    }

    fn recipes_before_sequence(&mut self, seq: &[usize]) -> usize {
        for i in 0.. {
            while i + seq.len() > self.recipes.len() {
                self.combine();
            }
            if &self.recipes[i..(i + seq.len())] == seq {
                return i;
            }
        }
        unreachable!()
    }
}

fn format_digits(digits: &[usize]) -> String {
    digits.iter().map(|c| c.to_string()).collect::<Vec<_>>().join("")
}

mod parser {
    use commons::parser::uint;
    use nom::types::CompleteStr;
    use nom::{eol, many1, map_res, named, take, terminated, verify, IResult};

    pub(super) fn input_as_number(i: CompleteStr) -> IResult<CompleteStr, usize> {
        terminated!(i, uint, eol)
    }

    named!(digit<CompleteStr, usize>,
       map_res!(
           verify!(take!(1), |c: CompleteStr| c.chars().next().unwrap().is_ascii_digit()),
           |c: CompleteStr| c.parse()
       )
    );

    named!(digits<CompleteStr, Vec<usize>>, many1!(digit));

    pub(super) fn input_as_digits(i: CompleteStr) -> IResult<CompleteStr, Vec<usize>> {
        terminated!(i, digits, eol)
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn test_parse_digits() {
            assert_eq!(
                vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                digits(CompleteStr(&"0123456789")).unwrap().1
            );
        }
    }
}

fn main() {
    let mut s = State::new();
    println!(
        "Ten scores after {}",
        format_digits(s.ten_scores_after(parse_input!(parser::input_as_number).unwrap()))
    );
    println!(
        "Recipes before sequences {}",
        s.recipes_before_sequence(&parse_input!(parser::input_as_digits).unwrap())
    );
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_ten_scores_after() {
        let mut s = State::new();
        assert_eq!(vec![0, 1, 2, 4, 5, 1, 5, 8, 9, 1], s.ten_scores_after(5));
        assert_eq!(vec![5, 1, 5, 8, 9, 1, 6, 7, 7, 9], s.ten_scores_after(9));
        assert_eq!(vec![9, 2, 5, 1, 0, 7, 1, 0, 8, 5], s.ten_scores_after(18));
        assert_eq!(vec![5, 9, 4, 1, 4, 2, 9, 8, 8, 2], s.ten_scores_after(2018));
    }

    #[test]
    fn test_recipes_before_sequence() {
        let mut s = State::new();
        assert_eq!(5, s.recipes_before_sequence(&[0, 1, 2, 4, 5]));
        assert_eq!(9, s.recipes_before_sequence(&[5, 1, 5, 8, 9]));
        assert_eq!(18, s.recipes_before_sequence(&[9, 2, 5, 1, 0]));
        assert_eq!(2018, s.recipes_before_sequence(&[5, 9, 4, 1, 4]));
    }

    #[test]
    fn test_format_digits() {
        assert_eq!("0123456789", format_digits(&[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]));
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;

    #[test]
    fn test_ten_scores_after() {
        let mut s = State::new();
        assert_eq!(
            "7861362411",
            format_digits(s.ten_scores_after(parse_input!(parser::input_as_number).unwrap()))
        );
    }

    #[test]
    fn test_recipes_before_sequence() {
        let mut s = State::new();
        assert_eq!(
            20_203_532,
            s.recipes_before_sequence(&parse_input!(parser::input_as_digits).unwrap())
        );
    }
}
