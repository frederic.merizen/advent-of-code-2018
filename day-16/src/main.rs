use commons::parse_input;
use maplit::hashmap;
use std::collections::HashMap;
use std::error::Error;
use std::fmt::{self, Display, Formatter};

fn main() {
    let (samples, code) = parse_input!(parser::problem_input).unwrap();
    println!(
        "Samples matching three or more opcodes: {}",
        count_samples_matching_3_opcodes_or_more(&samples)
    );
    println!(
        "R0 after running problem code {}",
        r0_after_code(&samples, &code).unwrap()
    );
}

fn r0_after_code<'a, S: IntoIterator<Item = &'a Sample>, C: IntoIterator<Item = &'a Instruction>>(
    samples: S,
    code: C,
) -> Result<Word, Unhandled> {
    let opcodes = find_opcode_numbers(samples)?;
    let mut regs = [0; 4];
    run(&opcodes, &mut regs, code);
    Ok(regs[0])
}

fn run<'a, T: IntoIterator<Item = &'a Instruction>>(
    opcodes: &HashMap<u8, Operation>,
    regs: &mut Registers,
    code: T,
) {
    for instruction in code {
        instruction.run(regs, opcodes);
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
struct Instruction {
    opcode: u8,
    input_a: u8,
    input_b: u8,
    output: u8,
}

impl Instruction {
    fn run(&self, regs: &mut Registers, opcodes: &HashMap<u8, Operation>) {
        opcodes[&self.opcode](regs, self.input_a, self.input_b, self.output);
    }
}

type Word = u16;

type Registers = [Word; 4];

type Operation = fn(&mut Registers, u8, u8, u8);

#[derive(Clone, Debug, Eq, PartialEq)]
struct Sample {
    before: Registers,
    instruction: Instruction,
    after: Registers,
}

impl Sample {
    fn matches_opcode(&self, op: Operation) -> bool {
        let mut r = self.before;
        op(
            &mut r,
            self.instruction.input_a,
            self.instruction.input_b,
            self.instruction.output,
        );
        r == self.after
    }

    fn count_matching_opcodes(&self) -> usize {
        self.keep_matching_opcodes(&all_operations()).iter().count()
    }

    fn keep_matching_opcodes<'a>(&self, opcodes: &HashMap<&'a str, Operation>) -> HashMap<&'a str, Operation> {
        opcodes
            .iter()
            .filter_map(|(&m, &o)| {
                if self.matches_opcode(o) {
                    Some((m, o))
                } else {
                    None
                }
            })
            .collect()
    }
}

fn count_samples_matching_3_opcodes_or_more<'a, T: IntoIterator<Item = &'a Sample>>(
    samples: T,
) -> usize {
    samples
        .into_iter()
        .filter(|s| s.count_matching_opcodes() >= 3)
        .count()
}

#[derive(Debug)]
struct Unhandled;

impl Display for Unhandled {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        "The input was too complex for this simplistic solver".fmt(fmt)
    }
}

impl Error for Unhandled {}

fn find_opcode_numbers<'a, T: IntoIterator<Item = &'a Sample>>(
    samples: T,
) -> Result<HashMap<u8, Operation>, Unhandled> {
    let mut compatible_opcodes = find_compatible_opcodes(samples);
    let mut single_opcodes = compatible_opcodes
        .iter()
        .filter_map(|(&n, ops)| {
            if ops.len() == 1 {
                Some((n, *ops.keys().next().unwrap()))
            } else {
                None
            }
        })
        .collect::<Vec<_>>();
    while let Some((single_n, single_op)) = single_opcodes.pop() {
        for (&n, ops) in compatible_opcodes.iter_mut().filter(|(&n, _)| n != single_n) {
            if ops.remove(single_op).is_some() && ops.len() == 1 {
                single_opcodes.push((n, ops.keys().next().unwrap()));
            }
        }
    }
    compatible_opcodes
        .into_iter()
        .map(|(n, ops)| {
            if ops.len() == 1 {
                Ok((n, *ops.values().next().unwrap()))
            } else {
                Err(Unhandled)
            }
        })
        .collect()
}

fn find_compatible_opcodes<'a, T: IntoIterator<Item = &'a Sample>>(
    samples: T,
) -> HashMap<u8, HashMap<&'static str, Operation>> {
    let mut compatible_opcodes_by_number = hashmap! {};
    for sample in samples {
        let opcodes = compatible_opcodes_by_number
            .entry(sample.instruction.opcode)
            .or_insert_with(all_operations);
        *opcodes = sample.keep_matching_opcodes(opcodes);
    }
    compatible_opcodes_by_number
}

macro_rules! def_operations {
    (fn $all: ident() =>
     $(fn $operation: ident($a1: ident, $a2: ident) {$op: ident})*) => {
        fn $all() -> HashMap<&'static str, Operation> {
            hashmap! { $(stringify!($operation) => $operation as Operation),* }
        }

        $(fn $operation(r: &mut Registers, i1: u8, i2: u8, o: u8) {
            r[o as usize] = $op($a1(*r, i1), $a2(*r, i2));
        })*
    };
}

def_operations! {
    fn all_operations() =>
    fn addr(getr, getr) {add}
    fn addi(getr, geti) {add}
    fn mulr(getr, getr) {mul}
    fn muli(getr, geti) {mul}
    fn banr(getr, getr) {ban}
    fn bani(getr, geti) {ban}
    fn borr(getr, getr) {bor}
    fn bori(getr, geti) {bor}
    fn setr(getr, geti) {set}
    fn seti(geti, geti) {set}
    fn gtir(geti, getr) {gt}
    fn gtri(getr, geti) {gt}
    fn gtrr(getr, getr) {gt}
    fn eqir(geti, getr) {eq}
    fn eqri(getr, geti) {eq}
    fn eqrr(getr, getr) {eq}
}

fn geti(_r: Registers, v: u8) -> Word {
    v.into()
}

fn getr(r: Registers, v: u8) -> Word {
    r[v as usize]
}

fn add(i1: Word, i2: Word) -> Word {
    i1 + i2
}

fn mul(i1: Word, i2: Word) -> Word {
    i1 * i2
}

fn ban(i1: Word, i2: Word) -> Word {
    i1 & i2
}

fn bor(i1: Word, i2: Word) -> Word {
    i1 | i2
}

fn set(i1: Word, _i2: Word) -> Word {
    i1
}

fn gt(i1: Word, i2: Word) -> Word {
    if i1 > i2 {
        1
    } else {
        0
    }
}

fn eq(i1: Word, i2: Word) -> Word {
    if i1 == i2 {
        1
    } else {
        0
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! assert_opcode {
        (before: $before: expr, $op: ident, in_a: $i1: expr, in_b: $i2: expr, out_c: $o: expr, after: $after: expr) => {
            let mut reg = $before;
            $op(&mut reg, $i1, $i2, $o);
            assert_eq!($after, reg);
        };
    }

    #[test]
    fn test_opcodes() {
        assert_opcode!(
            before: [1, 2, 4, 8],
            addr, in_a: 1, in_b: 2, out_c: 3,
            after: [1, 2, 4, 6]
        );
        assert_opcode!(
            before: [1, 2, 4, 8],
            addi, in_a: 3, in_b: 7, out_c: 0,
            after: [15, 2, 4, 8]
        );
        assert_opcode!(
            before: [3, 5, 0, 0],
            mulr, in_a: 0, in_b: 1, out_c: 2,
            after: [3, 5, 15, 0]
        );
        assert_opcode!(
            before: [3, 5, 0, 0],
            muli, in_a: 0, in_b: 2, out_c: 1,
            after: [3, 6, 0, 0]
        );
        assert_opcode!(
            before: [0xf8, 0x1f, 0, 0],
            banr, in_a: 0, in_b: 1, out_c: 1,
            after: [0xf8, 0x18, 0, 0]
        );
        assert_opcode!(
            before: [0xf8, 0x1f, 0xa5, 0x5a],
            bani, in_a: 2, in_b: 0x11, out_c: 1,
            after: [0xf8, 1, 0xa5, 0x5a]
        );
        assert_opcode!(
            before: [0xf0, 0x0f, 0, 0],
            borr, in_a: 0, in_b: 1, out_c: 3,
            after: [0xf0, 0x0f, 0, 0xff]
        );
        assert_opcode!(
            before: [0, 0, 0, 0xa5],
            bori, in_a: 3, in_b: 0x5a, out_c: 0,
            after: [0xff, 0, 0, 0xa5]
        );
        assert_opcode!(
            before: [0, 0, 0, 0xa5],
            setr, in_a: 3, in_b: 0x5a, out_c: 0,
            after: [0xa5, 0, 0, 0xa5]
        );
        assert_opcode!(
            before: [0, 0, 0, 0],
            seti, in_a: 3, in_b: 0x5a, out_c: 0,
            after: [3, 0, 0, 0]
        );
        assert_opcode!(
            before: [0, 4, 8, 10],
            gtir, in_a: 3, in_b: 0, out_c: 0,
            after: [1, 4, 8, 10]
        );
        assert_opcode!(
            before: [48, 24, 12, 6],
            gtri, in_a: 3, in_b: 10, out_c: 0,
            after: [0, 24, 12, 6]
        );
        assert_opcode!(
            before: [48, 24, 12, 6],
            gtrr, in_a: 2, in_b: 3, out_c: 1,
            after: [48, 1, 12, 6]
        );
        assert_opcode!(
            before: [0, 4, 8, 10],
            eqir, in_a: 8, in_b: 2, out_c: 0,
            after: [1, 4, 8, 10]
        );
        assert_opcode!(
            before: [48, 24, 10, 6],
            eqri, in_a: 2, in_b: 10, out_c: 0,
            after: [1, 24, 10, 6]
        );
        assert_opcode!(
            before: [48, 24, 6, 6],
            eqrr, in_a: 2, in_b: 3, out_c: 1,
            after: [48, 1, 6, 6]
        );
    }

    #[test]
    fn test_count_matching_opcodes() {
        let sample = Sample {
            before: [3, 2, 1, 1],
            instruction: Instruction {
                opcode: 9,
                input_a: 2,
                input_b: 1,
                output: 2,
            },
            after: [3, 2, 2, 1],
        };

        assert_eq!(3, sample.count_matching_opcodes());
    }

    #[test]
    fn test_count_samples_matching_3_opcodes_or_more() {
        let samples = vec![Sample {
            before: [3, 2, 1, 1],
            instruction: Instruction {
                opcode: 9,
                input_a: 2,
                input_b: 1,
                output: 2,
            },
            after: [3, 2, 2, 1],
        }];

        assert_eq!(1, count_samples_matching_3_opcodes_or_more(&samples));
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref INPUT: (Vec<Sample>, Vec<Instruction>) = parse_input!(parser::problem_input).unwrap();
    }

    fn samples() -> &'static Vec<Sample> {
        &INPUT.0
    }

    fn code() -> &'static Vec<Instruction> {
        &INPUT.1
    }

    #[test]
    fn test_count_samples_matching_3_opcodes_or_more() {
        assert_eq!(560, count_samples_matching_3_opcodes_or_more(samples()));
    }

    #[test]
    fn test_find_opcode_numbers() {
        let opcodes_by_num = find_opcode_numbers(samples()).unwrap();
        assert!(opcodes_by_num[&0x0] as usize == bani as usize);
        assert!(opcodes_by_num[&0x1] as usize == gtri as usize);
        assert!(opcodes_by_num[&0x2] as usize == seti as usize);
        assert!(opcodes_by_num[&0x3] as usize == eqir as usize);
        assert!(opcodes_by_num[&0x4] as usize == eqrr as usize);
        assert!(opcodes_by_num[&0x5] as usize == borr as usize);
        assert!(opcodes_by_num[&0x6] as usize == bori as usize);
        assert!(opcodes_by_num[&0x7] as usize == banr as usize);
        assert!(opcodes_by_num[&0x8] as usize == muli as usize);
        assert!(opcodes_by_num[&0x9] as usize == eqri as usize);
        assert!(opcodes_by_num[&0xa] as usize == mulr as usize);
        assert!(opcodes_by_num[&0xb] as usize == gtrr as usize);
        assert!(opcodes_by_num[&0xc] as usize == setr as usize);
        assert!(opcodes_by_num[&0xd] as usize == addr as usize);
        assert!(opcodes_by_num[&0xe] as usize == gtir as usize);
        assert!(opcodes_by_num[&0xf] as usize == addi as usize);
    }

    #[test]
    fn test_r0_after_code() {
        assert_eq!(622, r0_after_code(samples(), code()).unwrap());
    }
}

mod parser {
    use super::{Instruction, Registers, Sample};
    use commons::parser::uint;
    use nom::types::CompleteStr;
    use nom::{
        char, count, delimited, do_parse, eol, is_a, many0, named, opt, tag, terminated, IResult,
    };

    named!(space<CompleteStr, Option<CompleteStr>>, opt!(is_a!(" ")));
    named!(comma<CompleteStr, char>, delimited!(space, char!(','), space));

    named!(register_values<CompleteStr, Registers>,
           do_parse!(
                   char!('[') >>
               r0: uint       >>
                   comma      >>
               r1: uint       >>
                   comma      >>
               r2: uint       >>
                   comma      >>
               r3: uint       >>
                   char!(']') >>
               ([r0, r1, r2, r3])
           )
    );

    named!(instruction<CompleteStr, Instruction>,
           do_parse!(
               opcode:  uint  >>
                        space >>
               input_a: uint  >>
                        space >>
               input_b: uint  >>
                        space >>
               output:  uint  >>
               (Instruction{ opcode, input_a, input_b, output })
           )
    );

    named!(sample<CompleteStr, Sample>,
           do_parse!(
                            tag!("Before:") >>
                            space           >>
               before:      register_values >>
                            eol             >>
               instruction: instruction     >>
                            eol             >>
                            tag!("After:")  >>
                            space           >>
               after:       register_values >>
                            eol             >>
               (Sample{ before, instruction, after })
           )
    );

    #[rustfmt::skip]
    pub(super) fn problem_input(i: CompleteStr) -> IResult<CompleteStr, (Vec<Sample>, Vec<Instruction>)> {
           do_parse!(i,
               samples:      many0!(terminated!(sample, eol    ))      >>
                             count!(eol    , 2)                        >>
               instructions: many0!(terminated!(instruction, eol    )) >>
               (samples, instructions)
           )
    }

    #[cfg(test)]
    mod tests {
        use super::*;
        use indoc::indoc;

        #[test]
        fn test_register_values() {
            assert_eq!(
                Ok((CompleteStr(""), [1, 2, 3, 4])),
                register_values(CompleteStr("[1, 2, 3, 4]"))
            );
        }

        #[test]
        fn test_instruction() {
            assert_eq!(
                Ok((
                    CompleteStr(""),
                    Instruction {
                        opcode: 8,
                        input_a: 7,
                        input_b: 6,
                        output: 5
                    }
                )),
                instruction(CompleteStr("8 7 6 5"))
            );
        }

        #[test]
        fn test_sample() {
            assert_eq!(
                Ok((
                    CompleteStr(""),
                    Sample {
                        before: [11, 12, 13, 14],
                        instruction: Instruction {
                            opcode: 2,
                            input_a: 7,
                            input_b: 1,
                            output: 3,
                        },
                        after: [21, 22, 23, 24],
                    }
                )),
                sample(CompleteStr(indoc!(
                    "
                    Before: [11, 12, 13, 14]
                    2 7 1 3
                    After: [21, 22, 23, 24]
                    "
                )))
            );
        }

        #[test]
        fn test_problem_input() {
            assert_eq!(
                Ok((
                    CompleteStr(""),
                    (
                        vec![
                            Sample {
                                before: [11, 12, 13, 14],
                                instruction: Instruction {
                                    opcode: 2,
                                    input_a: 7,
                                    input_b: 1,
                                    output: 3,
                                },
                                after: [21, 22, 23, 24],
                            },
                            Sample {
                                before: [31, 32, 33, 34],
                                instruction: Instruction {
                                    opcode: 3,
                                    input_a: 0,
                                    input_b: 8,
                                    output: 4,
                                },
                                after: [41, 42, 43, 44],
                            }
                        ],
                        vec![
                            Instruction {
                                opcode: 9,
                                input_a: 8,
                                input_b: 7,
                                output: 6,
                            },
                            Instruction {
                                opcode: 15,
                                input_a: 4,
                                input_b: 3,
                                output: 2,
                            }
                        ]
                    )
                )),
                problem_input(CompleteStr(indoc!(
                    "
                    Before: [11, 12, 13, 14]
                    2 7 1 3
                    After: [21, 22, 23, 24]

                    Before: [31, 32, 33, 34]
                    3 0 8 4
                    After: [41, 42, 43, 44]



                    9 8 7 6
                    15 4 3 2
                    "
                )))
            );
        }
    }
}
