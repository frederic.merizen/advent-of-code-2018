use commons::parse_input;
use maplit::{hashmap, hashset};
use std::collections::hash_map::Entry;
use std::collections::{HashMap, HashSet, VecDeque};
use std::hash::Hash;
use Step::*;

fn main() {
    let route = parse_input!(parser::input).unwrap();
    let distances = route.dist_to_rooms();
    println!("Distance to furthest room: {}", distances.farthest_distance);
    println!(
        "Rooms more than 1000 away: {}",
        distances.count_rooms_beyond(1000)
    );
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
enum Step {
    North,
    East,
    South,
    West,
}

#[derive(Clone, Debug, Eq, PartialEq)]
enum Route {
    Step(Step),
    Seq(Vec<Route>),
    Alt(Vec<Route>),
}

impl Route {
    fn scan<F, L, G>(&self, l0: L, g0: G, f: &F) -> (HashSet<L>, G)
    where
        F: Fn(L, G, Step) -> (L, G),
        L: Clone + Eq + Hash,
    {
        use Route::*;

        match self {
            Step(s) => {
                let (l, g) = f(l0, g0, *s);
                (hashset! {l}, g)
            }
            Seq(rs) => rs.iter().fold((hashset! {l0}, g0), |(ls, g), r| {
                ls.into_iter().fold((hashset! {}, g), |(mut ls, g), l| {
                    let (nls, g) = r.scan(l, g, f);
                    ls.extend(nls);
                    (ls, g)
                })
            }),
            Alt(rs) => rs.iter().fold((hashset! {}, g0), |(mut ls, g), r| {
                let (nls, g) = r.scan(l0.clone(), g, f);
                ls.extend(nls);
                (ls, g)
            }),
        }
    }

    fn map_rooms(&self) -> HashMap<Room, HashSet<Room>> {
        self.scan(
            Room { x: 0, y: 0 },
            hashmap! {},
            &|from, mut rooms: HashMap<Room, HashSet<Room>>, s| {
                let to = from.neighbour(s);
                rooms.entry(from).or_default().insert(to);
                rooms.entry(to).or_default().insert(from);
                (to, rooms)
            },
        )
        .1
    }

    fn dist_to_rooms(&self) -> RoomDistances {
        let rooms = self.map_rooms();
        let mut distance_by_room = hashmap! {};
        let mut farthest_distance = 0;
        let mut next = VecDeque::new();
        next.push_back((0, Room { x: 0, y: 0 }));
        while !next.is_empty() {
            let (dist, room) = next.pop_front().unwrap();
            if let Entry::Vacant(e) = distance_by_room.entry(room) {
                e.insert(dist);
                farthest_distance = dist;
                next.extend(rooms[&room].iter().map(|r| (dist + 1, *r)));
            }
        }
        RoomDistances {
            farthest_distance,
            distance_by_room,
        }
    }
}

struct RoomDistances {
    farthest_distance: usize,
    distance_by_room: HashMap<Room, usize>,
}

impl RoomDistances {
    fn count_rooms_beyond(&self, dist: usize) -> usize {
        self.distance_by_room.iter().filter(|(_, &d)| d >= dist).count()
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
struct Room {
    x: isize,
    y: isize,
}

impl Room {
    fn neighbour(&self, s: Step) -> Self {
        let Room { x, y } = *self;
        match s {
            North => Room { x, y: y - 1 },
            East => Room { x: x + 1, y },
            South => Room { x, y: y + 1 },
            West => Room { x: x - 1, y },
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use nom::types::CompleteStr;

    #[test]
    fn test_map_rooms() {
        let room_se = Room { x: 0, y: 0 };
        let room_sw = Room { x: -1, y: 0 };
        let room_nw = Room { x: -1, y: -1 };
        let room_ne = Room { x: 0, y: -1 };
        assert_eq!(
            hashmap! {
                room_se => hashset!{room_sw},
                room_sw => hashset!{room_nw, room_se},
                room_nw => hashset!{room_ne, room_sw},
                room_ne => hashset!{room_nw},
            },
            parser::route(CompleteStr("^WNE$")).unwrap().1.map_rooms()
        );
        assert_eq!(
            hashmap! {
                room_se => hashset!{room_sw, room_ne},
                room_sw => hashset!{room_se, room_nw},
                room_nw => hashset!{room_sw},
                room_ne => hashset!{room_se},
            },
            parser::route(CompleteStr("^(WN|N)$")).unwrap().1.map_rooms()
        );
    }

    #[test]
    fn test_dist_to_rooms() {
        let room_se = Room { x: 0, y: 0 };
        let room_sw = Room { x: -1, y: 0 };
        let room_nw = Room { x: -1, y: -1 };
        let room_ne = Room { x: 0, y: -1 };
        assert_eq!(
            hashmap! {
                room_se => 0,
                room_sw => 1,
                room_nw => 2,
                room_ne => 3,
            },
            parser::route(CompleteStr("^WNE$"))
                .unwrap()
                .1
                .dist_to_rooms()
                .distance_by_room
        );
        assert_eq!(
            hashmap! {
                room_se => 0,
                room_sw => 1,
                room_nw => 2,
                room_ne => 1,
            },
            parser::route(CompleteStr("^(WN|N)$"))
                .unwrap()
                .1
                .dist_to_rooms()
                .distance_by_room
        );
    }

    #[test]
    fn test_max_dist_to_rooms() {
        assert_eq!(
            3,
            parser::route(CompleteStr("^WNE$"))
                .unwrap()
                .1
                .dist_to_rooms()
                .farthest_distance
        );
        assert_eq!(
            2,
            parser::route(CompleteStr("^(WN|N)$"))
                .unwrap()
                .1
                .dist_to_rooms()
                .farthest_distance
        );
    }
}

mod parser {
    use super::Route;
    use super::Route::*;
    use super::Step::*;
    use nom::types::CompleteStr;
    use nom::{
        alt, char, delimited, eol, many0, many1, map, named, pair, preceded, terminated, value, IResult,
    };

    named! {step<CompleteStr, Route>,
        map!(
            alt!(
                value!(North, char!('N')) |
                value!(East, char!('E')) |
                value!(South, char!('S')) |
                value!(West, char!('W'))
            ),
            Step
        )
    }

    named! {sequence<CompleteStr, Route>,
        map!(many0!(alt!(step | branch)), smart_seq)
    }

    fn smart_seq(mut routes: Vec<Route>) -> Route {
        // As a quick and dirty way to make an unambiguous, non left-recursive grammar,
        // we parse the input with a generous helping of Seq on top.
        //
        // This clean-up function removes the superfluous Seqs.
        if routes.len() == 1 {
            routes.pop().unwrap()
        } else {
            Seq(routes)
        }
    }

    fn to_alt((route, mut routes): (Route, Vec<Route>)) -> Route {
        routes.insert(0, route);
        Alt(routes)
    }

    named! {branch<CompleteStr, Route>,
        delimited!(
            char!('('),
            map!(
                pair!(
                    sequence,
                    many1!(preceded!(char!('|'), sequence))
                ),
                to_alt
            ),
            char!(')')
        )
    }

    pub(super) fn route(i: CompleteStr) -> IResult<CompleteStr, Route> {
        delimited!(i, char!('^'), sequence, char!('$'))
    }

    pub(super) fn input(i: CompleteStr) -> IResult<CompleteStr, Route> {
        terminated!(i, route, eol)
    }

    #[cfg(test)]
    #[allow(clippy::deref_addrof)]
    mod tests {
        use super::*;
        use lazy_static::lazy_static;

        fn parse_route(r: &str) -> Route {
            match route(CompleteStr(r)) {
                Ok(r) => r.1,
                Err(r) => panic!("{:?}", r),
            }
        }

        lazy_static! {
            static ref NORTH: Route = { Step(North) };
            static ref EAST: Route = { Step(East) };
            static ref SOUTH: Route = { Step(South) };
            static ref WEST: Route = { Step(West) };
        }

        macro_rules! seq {
            ( $( $x:expr ),* ) => {
                Seq(vec![$((*$x).clone()),*])
            };
        }

        macro_rules! alt {
            ( $( $x:expr ),* ) => {
                Alt(vec![$((*$x).clone()),*])
            };
        }

        #[test]
        fn parse_single_step_route() {
            assert_eq!(*EAST, parse_route("^E$"));
        }

        #[test]
        fn parse_multi_step_route() {
            assert_eq!(seq![EAST, NORTH, WEST], parse_route("^ENW$"));
        }

        #[test]
        fn parse_alt() {
            assert_eq!(alt!(EAST, WEST), parse_route("^(E|W)$"));
        }

        #[test]
        fn parse_empty() {
            assert_eq!(seq![], parse_route("^$"));
        }

        #[test]
        fn parse_complexish_routes() {
            assert_eq!(
                seq![
                    EAST,
                    NORTH,
                    NORTH,
                    WEST,
                    SOUTH,
                    WEST,
                    WEST,
                    &alt!(&seq![NORTH, EAST, WEST, SOUTH], &seq![]),
                    SOUTH,
                    SOUTH,
                    SOUTH,
                    EAST,
                    EAST,
                    NORTH,
                    &alt!(&seq![WEST, NORTH, SOUTH, EAST], &seq![]),
                    EAST,
                    EAST,
                    &alt!(&seq![SOUTH, WEST, EAST, NORTH], &seq![]),
                    NORTH,
                    NORTH,
                    NORTH
                ],
                parse_route("^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$")
            );
        }
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref DISTANCES: RoomDistances = {
            let route = parse_input!(parser::input).unwrap();
            route.dist_to_rooms()
        };
    }

    #[test]
    fn test_farthest_distance() {
        assert_eq!(3839, DISTANCES.farthest_distance);
    }

    #[test]
    fn test_count_rooms_beyond() {
        assert_eq!(8407, DISTANCES.count_rooms_beyond(1000));
    }
}
