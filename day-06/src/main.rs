use commons::{lines, parse_input, Result};
use maplit::{hashmap, hashset};
use std::cmp::{max, min};
use std::collections::{HashMap, HashSet};
use std::ops::{Index, IndexMut};
use ClaimId::*;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
struct Point(isize, isize);

impl Point {
    fn dist(&self, &Point(ox, oy): &Point) -> usize {
        let Point(x, y) = self;
        ((x - ox).abs() + (y - oy).abs()) as usize
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
struct ZoneId(u16);

#[derive(Clone, PartialEq, Eq, Debug)]
enum ClaimId {
    Unclaimed,
    Claimed(ZoneId),
    Conflict,
}

mod parser {
    use super::Point;
    use commons::parser::uint;
    use nom::types::CompleteStr;
    use nom::{do_parse, tag, IResult};

    #[rustfmt::skip]
    pub(super) fn line(i: CompleteStr) -> IResult<CompleteStr, Point> {
        do_parse!(i,
           x: uint       >>
              tag!(", ") >>
           y: uint       >>
           (Point(x, y))
        )
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        fn parse_line(s: &str) -> IResult<CompleteStr, Point> {
            line(CompleteStr(s))
        }

        #[test]
        fn test_parse_line() {
            assert_eq!(Point(35, 246), parse_line("35, 246").unwrap().1);
            assert_eq!(Point(234, 777), parse_line("234, 777").unwrap().1);
            assert!(parse_line("XVI, 777").is_err());
        }
    }
}

fn read_input<'a>() -> Result<'a, Vec<Point>> {
    parse_input!(lines!(parser::line))
}

struct Field {
    top_left: Point,
    bottom_right: Point,
    w: isize,
    points: Vec<ClaimId>,
}

impl Field {
    fn new(top_left: Point, bottom_right: Point) -> Self {
        let min_x = top_left.0 - 1;
        let min_y = top_left.1 - 1;
        let max_x = bottom_right.0 + 1;
        let max_y = bottom_right.1 + 1;
        let w = max_x - min_x;
        let mut res = Field {
            top_left,
            bottom_right,
            w,
            points: vec![Unclaimed; (w * (max_y - min_y + 1) + 1) as usize],
        };
        for x in min_x..max_x {
            res[Point(x, min_y)] = Conflict;
            res[Point(x, max_y)] = Conflict;
        }
        for y in min_y..=max_y {
            res[Point(max_x, y)] = Conflict;
        }
        res
    }

    fn infinite_zones(&self) -> HashSet<ZoneId> {
        let mut result = hashset! {};
        let Point(min_x, min_y) = self.top_left;
        let Point(max_x, max_y) = self.bottom_right;

        for x in min_x..=max_x {
            if let Claimed(z) = self[Point(x, min_y)] {
                result.insert(z);
            }
            if let Claimed(z) = self[Point(x, max_y)] {
                result.insert(z);
            }
        }

        for y in (min_y + 1)..max_y {
            if let Claimed(z) = self[Point(min_x, y)] {
                result.insert(z);
            }
            if let Claimed(z) = self[Point(max_x, y)] {
                result.insert(z);
            }
        }

        result
    }

    fn zone_areas(&self) -> HashMap<ZoneId, usize> {
        let mut result = hashmap! {};

        let Point(min_x, min_y) = self.top_left;
        let Point(max_x, max_y) = self.bottom_right;

        for y in (min_y + 1)..max_y {
            for x in (min_x + 1)..max_x {
                if let Claimed(z) = self[Point(x, y)] {
                    let e = result.entry(z).or_insert(0);
                    *e += 1;
                }
            }
        }
        result
    }

    fn finite_zone_areas(&self) -> HashMap<ZoneId, usize> {
        let infinite = self.infinite_zones();
        let mut areas = self.zone_areas();

        for z in infinite.iter() {
            areas.remove(z);
        }

        areas
    }

    fn largest_finite_zone_area(&self) -> Option<usize> {
        Some(*self.finite_zone_areas().values().max()?)
    }
}

impl Index<Point> for Field {
    type Output = ClaimId;

    fn index(&self, Point(x, y): Point) -> &ClaimId {
        assert!(x >= self.top_left.0 - 1);
        assert!(y >= self.top_left.1 - 1);
        assert!(x <= self.bottom_right.0 + 1);
        assert!(y <= self.bottom_right.1 + 1);
        &self.points[(x + 1 - self.top_left.0 + (y + 1 - self.top_left.1) * self.w) as usize]
    }
}

impl IndexMut<Point> for Field {
    fn index_mut(&mut self, Point(x, y): Point) -> &mut ClaimId {
        assert!(x >= self.top_left.0 - 1);
        assert!(y >= self.top_left.1 - 1);
        assert!(x <= self.bottom_right.0 + 1);
        assert!(y <= self.bottom_right.1 + 1);
        &mut self.points[(x + 1 - self.top_left.0 + (y + 1 - self.top_left.1) * self.w) as usize]
    }
}

fn bounding_box(points: &[Point]) -> (Point, Point) {
    points.iter().fold(
        (
            Point(std::isize::MAX, std::isize::MAX),
            Point(std::isize::MIN, std::isize::MIN),
        ),
        |(Point(x_min, y_min), Point(x_max, y_max)), &Point(x, y)| {
            (
                Point(min(x_min, x), min(y_min, y)),
                Point(max(x_max, x), max(y_max, y)),
            )
        },
    )
}

struct ExplorationState {
    field: Field,
    current: HashSet<(ZoneId, Point)>,
    next: HashSet<(ZoneId, Point)>,
}

impl ExplorationState {
    fn new(points: &[Point]) -> Self {
        let (top_left, bottom_right) = bounding_box(points);
        let field = Field::new(top_left, bottom_right);
        let current = (1..).map(ZoneId).zip(points.iter().cloned()).collect();
        ExplorationState {
            field,
            current,
            next: hashset! {},
        }
    }

    fn explore_step(&mut self) {
        for (z, p) in self.current.iter() {
            let s = &mut self.field[*p];
            match *s {
                Unclaimed => {
                    *s = Claimed(*z);
                }
                Claimed(_) => {
                    *s = Conflict;
                }
                _ => {}
            }
        }
        self.next.clear();
        for &(z, p) in self.current.iter() {
            let Point(x, y) = p;
            for &(dx, dy) in [(1, 0), (0, 1), (-1, 0), (0, -1)].iter() {
                let p2 = Point(x + dx, y + dy);
                if let Unclaimed = self.field[p2] {
                    self.next.insert((z, p2));
                }
            }
        }
        std::mem::swap(&mut self.current, &mut self.next);
    }

    fn explore_all(&mut self) {
        while !self.current.is_empty() {
            self.explore_step();
        }
    }
}

fn largest_finite_zone_area(points: &[Point]) -> Option<usize> {
    let mut state = ExplorationState::new(points);
    state.explore_all();
    state.field.largest_finite_zone_area()
}

fn area_below_cumulated_distance(points: &[Point], max_dist: usize) -> usize {
    let offset = (max_dist / points.len()) as isize;
    let (Point(min_x, min_y), Point(max_x, max_y)) = bounding_box(points);

    ((min_x - offset)..=(max_x + offset))
        .map(|x| {
            ((min_y - offset)..=(max_y + offset))
                .filter(|&y| {
                    let p1 = Point(x, y);
                    points.iter().map(|p2| p1.dist(p2)).sum::<usize>() < max_dist
                })
                .count()
        })
        .sum()
}

fn main() {
    let points = read_input().unwrap();
    println!("Part 1: {}", largest_finite_zone_area(&points).unwrap());
    println!("Part 2: {}", area_below_cumulated_distance(&points, 10_000));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new_field() {
        let f = Field::new(Point(2, 4), Point(3, 5));
        assert_eq!(Unclaimed, f[Point(2, 4)]);
        assert_eq!(Unclaimed, f[Point(2, 5)]);
        assert_eq!(Unclaimed, f[Point(3, 4)]);
        assert_eq!(Unclaimed, f[Point(3, 5)]);
    }

    #[test]
    fn test_new_field_sentinels() {
        let f = Field::new(Point(2, 4), Point(3, 5));
        assert_eq!(Conflict, f[Point(1, 3)]);
        assert_eq!(Conflict, f[Point(2, 3)]);
        assert_eq!(Conflict, f[Point(3, 3)]);
        assert_eq!(Conflict, f[Point(4, 3)]);
        assert_eq!(Conflict, f[Point(1, 4)]);
        assert_eq!(Conflict, f[Point(4, 4)]);
        assert_eq!(Conflict, f[Point(1, 5)]);
        assert_eq!(Conflict, f[Point(4, 5)]);
        assert_eq!(Conflict, f[Point(1, 6)]);
        assert_eq!(Conflict, f[Point(2, 6)]);
        assert_eq!(Conflict, f[Point(3, 6)]);
        assert_eq!(Conflict, f[Point(4, 6)]);
    }

    #[test]
    fn test_bounding_box() {
        assert_eq!(
            (Point(1, 5), Point(18, 30)),
            bounding_box(&[
                Point(1, 6),
                Point(3, 5),
                Point(18, 9),
                Point(7, 30),
                Point(12, 25)
            ])
        );
    }

    #[test]
    fn test_explore_one_step() {
        let mut s = ExplorationState::new(&[Point(0, 0), Point(11, 11), Point(5, 5)]);

        s.explore_step();

        assert_eq!(Claimed(ZoneId(3)), s.field[Point(5, 5)]);
    }

    #[test]
    fn test_explore_two_steps() {
        let mut s = ExplorationState::new(&[Point(0, 0), Point(11, 11), Point(5, 5)]);

        s.explore_step();
        s.explore_step();

        assert_eq!(Claimed(ZoneId(3)), s.field[Point(4, 5)]);
        assert_eq!(Claimed(ZoneId(3)), s.field[Point(5, 4)]);
        assert_eq!(Claimed(ZoneId(3)), s.field[Point(6, 5)]);
        assert_eq!(Claimed(ZoneId(3)), s.field[Point(5, 6)]);
    }

    #[test]
    fn test_explore_three_steps() {
        let mut s = ExplorationState::new(&[Point(0, 0), Point(11, 11), Point(5, 5)]);

        s.explore_step();
        s.explore_step();
        s.explore_step();

        assert_eq!(Claimed(ZoneId(3)), s.field[Point(3, 5)]);
        assert_eq!(Claimed(ZoneId(3)), s.field[Point(4, 4)]);
        assert_eq!(Claimed(ZoneId(3)), s.field[Point(5, 3)]);
        assert_eq!(Claimed(ZoneId(3)), s.field[Point(6, 4)]);
        assert_eq!(Claimed(ZoneId(3)), s.field[Point(7, 5)]);
        assert_eq!(Claimed(ZoneId(3)), s.field[Point(6, 6)]);
        assert_eq!(Claimed(ZoneId(3)), s.field[Point(5, 7)]);
        assert_eq!(Claimed(ZoneId(3)), s.field[Point(4, 6)]);
    }

    #[test]
    fn test_explore_conflict() {
        let mut s = ExplorationState::new(&[Point(0, 0), Point(0, 2)]);

        s.explore_step();
        s.explore_step();

        assert_eq!(Conflict, s.field[Point(0, 1)]);
    }

    fn testing_field() -> Field {
        let mut f = Field::new(Point(0, 0), Point(5, 2));

        f[Point(0, 0)] = Claimed(ZoneId(1));
        f[Point(1, 0)] = Claimed(ZoneId(1));
        f[Point(2, 0)] = Claimed(ZoneId(2));
        f[Point(3, 0)] = Claimed(ZoneId(2));
        f[Point(4, 0)] = Claimed(ZoneId(2));
        f[Point(5, 0)] = Claimed(ZoneId(2));

        f[Point(0, 1)] = Claimed(ZoneId(1));
        f[Point(1, 1)] = Claimed(ZoneId(3));
        f[Point(2, 1)] = Claimed(ZoneId(5));
        f[Point(3, 1)] = Claimed(ZoneId(5));
        f[Point(4, 1)] = Claimed(ZoneId(4));
        f[Point(5, 1)] = Claimed(ZoneId(4));

        f[Point(0, 2)] = Claimed(ZoneId(1));
        f[Point(1, 2)] = Claimed(ZoneId(2));
        f[Point(2, 2)] = Claimed(ZoneId(2));
        f[Point(3, 2)] = Claimed(ZoneId(2));
        f[Point(4, 2)] = Claimed(ZoneId(2));
        f[Point(5, 2)] = Claimed(ZoneId(2));

        f
    }

    #[test]
    fn test_infinite_zones() {
        assert_eq!(
            hashset! {ZoneId(1), ZoneId(2), ZoneId(4)},
            testing_field().infinite_zones()
        );
    }

    #[test]
    fn test_areas() {
        assert_eq!(
            hashmap! {ZoneId(3) => 1, ZoneId(4) => 1, ZoneId(5) => 2},
            testing_field().zone_areas()
        );
    }

    #[test]
    fn test_finite_areas() {
        assert_eq!(
            hashmap! {ZoneId(3) => 1, ZoneId(5) => 2},
            testing_field().finite_zone_areas()
        );
    }

    #[test]
    fn test_largest_finite_area() {
        assert_eq!(Some(2), testing_field().largest_finite_zone_area());
    }

    #[test]
    fn test_dist() {
        assert_eq!(0, Point(5, 5).dist(&Point(5, 5)));
        assert_eq!(5, Point(0, 5).dist(&Point(5, 5)));
        assert_eq!(5, Point(5, 5).dist(&Point(5, 0)));
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref POINTS: Vec<Point> = read_input().unwrap();
    }

    #[test]
    fn test_largest_finite_zone_area() {
        assert_eq!(3620, largest_finite_zone_area(&*POINTS).unwrap());
    }

    #[test]
    fn test_area_below_cumulated_distance() {
        assert_eq!(39930, area_below_cumulated_distance(&*POINTS, 10_000));
    }
}
