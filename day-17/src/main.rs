use commons::{parse_input, OrInvalid, Result};
#[cfg(test)]
use std::fmt::Write;
#[cfg(test)]
use std::iter::repeat;
use std::ops::{Index, IndexMut, RangeInclusive};
use ClayVein::*;
use Square::*;

fn main() {
    let mut ground = parse_input!(parser::ground).unwrap();
    ground.flow();
    println!("Wet squares: {}", ground.wet_squares());
    println!("Still water squares: {}", ground.still_water_squares());
}

#[derive(Debug, Eq, PartialEq)]
enum ClayVein {
    Vertical { x: usize, y: RangeInclusive<usize> },
    Horizontal { x: RangeInclusive<usize>, y: usize },
}

// Flowing water not strictly needed to solve the problem, but it looks good in dumps
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum Square {
    Sand,
    Clay,
    FlowingWater,
    StillWater,
}

impl Square {
    #[cfg(test)]
    fn to_char(self) -> char {
        match self {
            Sand => '.',
            Clay => '#',
            FlowingWater => '|',
            StillWater => '~',
        }
    }

    fn is_permeable(self) -> bool {
        !self.is_support()
    }

    fn is_support(self) -> bool {
        match self {
            Clay | StillWater => true,
            Sand | FlowingWater => false,
        }
    }

    fn is_wet(self) -> bool {
        match self {
            FlowingWater | StillWater => true,
            Sand | Clay => false,
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
struct Ground {
    x_min: usize,
    x_max: usize,
    y_min: usize,
    y_max: usize,
    field: Vec<Square>,
}

impl Index<[usize; 2]> for Ground {
    type Output = Square;

    fn index(&self, [x, y]: [usize; 2]) -> &Square {
        &self.field[x - self.x_min + (y - self.y_min) * (self.x_max - self.x_min + 1)]
    }
}

impl IndexMut<[usize; 2]> for Ground {
    fn index_mut(&mut self, [x, y]: [usize; 2]) -> &mut Square {
        &mut self.field[x - self.x_min + (y - self.y_min) * (self.x_max - self.x_min + 1)]
    }
}

impl Ground {
    fn from<'a>(clay_veins: Vec<ClayVein>) -> Result<'a, Ground> {
        let x_min = clay_veins
            .iter()
            .map(ClayVein::x_min)
            .min()
            .map(|m| m - 1)
            .or_invalid()?;
        let x_max = clay_veins
            .iter()
            .map(ClayVein::x_max)
            .max()
            .map(|m| m + 1)
            .or_invalid()?;
        let y_min = clay_veins.iter().map(ClayVein::y_min).min().or_invalid()?;
        let y_max = clay_veins.iter().map(ClayVein::y_max).max().or_invalid()?;
        let mut ground = Ground {
            x_min,
            x_max,
            y_min,
            y_max,
            field: vec![Sand; (x_max - x_min + 1) * y_max],
        };

        for vein in clay_veins {
            match vein {
                Horizontal { x, y } => {
                    for x in x {
                        ground[[x, y]] = Clay;
                    }
                }
                Vertical { x, y } => {
                    for y in y {
                        ground[[x, y]] = Clay;
                    }
                }
            }
        }

        Ok(ground)
    }

    #[cfg(test)]
    fn dump(&self) -> String {
        fn column_header(res: &mut String, left_col_width: usize, ground: &Ground) {
            let top_row_height = (ground.x_max as f64).log10() as usize + 1;

            for l in 1..=top_row_height {
                res.extend(repeat(' ').take(left_col_width));
                let divider = 10u32.pow((top_row_height - l) as u32);
                for x in ground.x_min..=ground.x_max {
                    res.push(std::char::from_digit((x as u32 / divider) % 10, 10).unwrap());
                }
                res.push('\n');
            }
        }

        fn left_col(res: &mut String, left_col_width: usize, y: usize) {
            write!(res, "{:width$} ", y, width = left_col_width - 1).unwrap();
        }

        fn water_spring_line(res: &mut String, left_col_width: usize, ground: &Ground) {
            left_col(res, left_col_width, ground.y_min - 1);
            res.extend(repeat('.').take(500 - ground.x_min));
            res.push('+');
            res.extend(repeat('.').take(ground.x_max - 500));
            res.push('\n');
        }

        fn field(res: &mut String, left_col_width: usize, ground: &Ground) {
            let mut squares = ground.field.iter();
            for y in ground.y_min..=ground.y_max {
                left_col(res, left_col_width, y);
                for _x in ground.x_min..=ground.x_max {
                    res.push(squares.next().unwrap().to_char());
                }
                res.push('\n');
            }
        }

        let mut res: String = "".into();
        let left_col_width = (self.y_max as f64).log10() as usize + 2;

        column_header(&mut res, left_col_width, self);
        water_spring_line(&mut res, left_col_width, self);
        field(&mut res, left_col_width, self);

        res
    }

    fn flow(&mut self) {
        fn flow_sideways<F: Fn(usize) -> usize>(
            ground: &Ground,
            mut x: usize,
            y: usize,
            step: F,
        ) -> usize {
            while ground[[x, y + 1]].is_support() && ground[[step(x), y]].is_permeable() {
                x = step(x)
            }

            x
        }

        let mut taps = vec![[500, self.y_min - 1]];
        while !taps.is_empty() {
            let [x, mut y] = taps.pop().unwrap();

            while y < self.y_max && self[[x, y + 1]].is_permeable() {
                y += 1;
                self[[x, y]] = FlowingWater;
            }

            if y == self.y_max {
                continue;
            }

            let left_x = flow_sideways(self, x, y, |x| x - 1);
            let right_x = flow_sideways(self, x, y, |x| x + 1);

            let left_supported = self[[left_x, y + 1]].is_support();
            let right_supported = self[[right_x, y + 1]].is_support();

            let filler;
            if left_supported && right_supported {
                filler = StillWater;
                taps.push([x, y - 1]);
            } else {
                filler = FlowingWater;
                if !left_supported {
                    taps.push([left_x, y]);
                }
                if !right_supported {
                    taps.push([right_x, y]);
                }
            }

            for x in left_x..=right_x {
                self[[x, y]] = filler;
            }
        }
    }

    fn wet_squares(&self) -> usize {
        self.field.iter().filter(|s| s.is_wet()).count()
    }

    fn still_water_squares(&self) -> usize {
        self.field.iter().filter(|&&s| s == StillWater).count()
    }
}

impl ClayVein {
    fn x_min(&self) -> usize {
        *match self {
            Vertical { x, .. } => x,
            Horizontal { x, .. } => x.start(),
        }
    }

    fn x_max(&self) -> usize {
        *match self {
            Vertical { x, .. } => x,
            Horizontal { x, .. } => x.end(),
        }
    }

    fn y_min(&self) -> usize {
        *match self {
            Vertical { y, .. } => y.start(),
            Horizontal { y, .. } => y,
        }
    }

    fn y_max(&self) -> usize {
        *match self {
            Vertical { y, .. } => y.end(),
            Horizontal { y, .. } => y,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use indoc::indoc;
    use nom::types::CompleteStr;

    #[test]
    fn test_horizontal_vein_bounds() {
        let vein = Horizontal { x: 2..=53, y: 99 };
        assert_eq!(2, vein.x_min());
        assert_eq!(53, vein.x_max());
        assert_eq!(99, vein.y_min());
        assert_eq!(99, vein.y_max());
    }

    #[test]
    fn test_vertical_vein_bounds() {
        let vein = Vertical { x: 5, y: 3..=17 };
        assert_eq!(5, vein.x_min());
        assert_eq!(5, vein.x_max());
        assert_eq!(3, vein.y_min());
        assert_eq!(17, vein.y_max());
    }

    #[test]
    fn test_ground_bounds() {
        let ground = Ground::from(vec![
            Horizontal { x: 15..=21, y: 2 },
            Vertical { x: 23, y: 18..=77 },
        ])
        .unwrap();
        assert_eq!(14, ground.x_min);
        assert_eq!(24, ground.x_max);
        assert_eq!(2, ground.y_min);
        assert_eq!(77, ground.y_max);
    }

    #[test]
    fn test_dump() {
        let (_, ground) = parser::ground(CompleteStr(indoc!(
            "
            x=495, y=2..7
            y=7, x=495..501
            x=501, y=3..7
            x=498, y=2..4
            x=506, y=1..2
            x=498, y=10..13
            x=504, y=10..13
            y=13, x=498..504
            "
        )))
        .unwrap();

        assert_eq!(
            indoc!(
                "
                   44444455555555
                   99999900000000
                   45678901234567
                 0 ......+.......
                 1 ............#.
                 2 .#..#.......#.
                 3 .#..#..#......
                 4 .#..#..#......
                 5 .#.....#......
                 6 .#.....#......
                 7 .#######......
                 8 ..............
                 9 ..............
                10 ....#.....#...
                11 ....#.....#...
                12 ....#.....#...
                13 ....#######...
                "
            ),
            ground.dump()
        );
    }

    #[test]
    fn test_flow_down() {
        let (_, mut ground) = parser::ground(CompleteStr(indoc!(
            "
            x=495, y=2..7
            x=505, y=2..7
            "
        )))
        .unwrap();

        ground.flow();

        assert_eq!(
            indoc!(
                "
                  4444445555555
                  9999990000000
                  4567890123456
                1 ......+......
                2 .#....|....#.
                3 .#....|....#.
                4 .#....|....#.
                5 .#....|....#.
                6 .#....|....#.
                7 .#....|....#.
                "
            ),
            ground.dump()
        );

        assert_eq!(6, ground.wet_squares());
        assert_eq!(0, ground.still_water_squares());
    }

    #[test]
    fn test_deflection() {
        let (_, mut ground) = parser::ground(CompleteStr(indoc!(
            "
            x=495, y=2..7
            x=505, y=2..7
            y=5, x=498..502
            "
        )))
        .unwrap();

        ground.flow();

        assert_eq!(
            indoc!(
                "
                  4444445555555
                  9999990000000
                  4567890123456
                1 ......+......
                2 .#....|....#.
                3 .#....|....#.
                4 .#.|||||||.#.
                5 .#.|#####|.#.
                6 .#.|.....|.#.
                7 .#.|.....|.#.
                "
            ),
            ground.dump()
        );

        assert_eq!(15, ground.wet_squares());
        assert_eq!(0, ground.still_water_squares());
    }

    #[test]
    fn test_fill_basins() {
        let (_, mut ground) = parser::ground(CompleteStr(indoc!(
            "
            x=495, y=2..7
            x=505, y=2..7
            y=5, x=498..502
            x=498, y=4..5
            x=502, y=4..5
            "
        )))
        .unwrap();

        ground.flow();

        assert_eq!(
            indoc!(
                "
                  4444445555555
                  9999990000000
                  4567890123456
                1 ......+......
                2 .#....|....#.
                3 .#.|||||||.#.
                4 .#.|#~~~#|.#.
                5 .#.|#####|.#.
                6 .#.|.....|.#.
                7 .#.|.....|.#.
                "
            ),
            ground.dump()
        );

        assert_eq!(19, ground.wet_squares());
        assert_eq!(3, ground.still_water_squares());
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref GROUND: Ground = {
            let mut ground = parse_input!(parser::ground).unwrap();
            ground.flow();
            ground
        };
    }

    #[test]
    fn test_wet_squares() {
        assert_eq!(27736, GROUND.wet_squares());
    }

    #[test]
    fn test_still_water_squares() {
        assert_eq!(22474, GROUND.still_water_squares());
    }
}

mod parser {
    use super::ClayVein::*;
    use super::{ClayVein, Ground};
    use commons::parser::uint;
    use nom::types::CompleteStr;
    use nom::{alt, do_parse, eol, many0, map_res, named, tag, terminated, IResult};

    named!(vertical_vein<CompleteStr, ClayVein>,
           do_parse!(
                   tag!("x=")   >>
               x:  uint         >>
                   tag!(", y=") >>
               y1: uint         >>
                   tag!("..")   >>
               y2: uint         >>
               (Vertical{ x, y: y1..=y2 })
           )
    );

    named!(horizontal_vein<CompleteStr, ClayVein>,
           do_parse!(
                   tag!("y=")   >>
               y:  uint         >>
                   tag!(", x=") >>
               x1: uint         >>
                   tag!("..")   >>
               x2: uint         >>
               (Horizontal{ y, x: x1..=x2 })
           )
    );

    named!(vein<CompleteStr, ClayVein>, alt!(vertical_vein | horizontal_vein));

    pub(super) fn ground(i: CompleteStr) -> IResult<CompleteStr, Ground> {
        map_res!(i, many0!(terminated!(vein, eol)), Ground::from)
    }

    #[cfg(test)]
    mod tests {
        use super::*;
        use indoc::indoc;

        #[test]
        fn test_parse_vertical_vein() {
            assert_eq!(
                Ok((CompleteStr(""), Vertical { x: 495, y: 2..=7 })),
                vertical_vein(CompleteStr("x=495, y=2..7"))
            );
        }

        #[test]
        fn test_parse_horizontal_vein() {
            assert_eq!(
                Ok((CompleteStr(""), Horizontal { x: 495..=501, y: 7 })),
                horizontal_vein(CompleteStr("y=7, x=495..501"))
            );
        }

        #[test]
        fn test_parse_vein() {
            assert_eq!(
                Ok((CompleteStr(""), Vertical { x: 495, y: 2..=7 })),
                vein(CompleteStr("x=495, y=2..7"))
            );
            assert_eq!(
                Ok((CompleteStr(""), Horizontal { x: 495..=501, y: 7 })),
                vein(CompleteStr("y=7, x=495..501"))
            )
        }

        #[test]
        fn test_parse_ground() {
            assert_eq!(
                Ok((
                    CompleteStr(""),
                    Ground::from(vec![
                        Vertical { x: 495, y: 2..=7 },
                        Horizontal { x: 495..=501, y: 7 }
                    ])
                    .unwrap()
                )),
                ground(CompleteStr(indoc!(
                    "
                    x=495, y=2..7
                    y=7, x=495..501
                    "
                )))
            );
        }
    }
}
